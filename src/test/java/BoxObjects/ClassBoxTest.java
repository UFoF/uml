package BoxObjects;

import Relationships.Aggregation;
import Relationships.Relationship;
import javafx.embed.swing.JFXPanel;
import junit.framework.TestCase;
import ThreadingRule.JavaFXThreadingRule;
import BoxObjects.ClassBox;
import javafx.scene.control.TextArea;
import org.junit.Rule;

import java.util.ArrayList;

/**
 * Created by UFOF on 4/30/15.
 */
public class ClassBoxTest extends TestCase {
    @Rule
    JavaFXThreadingRule rule = new JavaFXThreadingRule();

    /**
     *
     * @throws Exception
     * Create two class boxes and one relationship,
     * then add the relationship to the class boxes.
     */
    @org.junit.Test
    public void testAddRelationship() throws Exception {
        new JFXPanel();
        ClassBox testBox1 = new ClassBox(6, 9);
        ClassBox testBox2 = new ClassBox(666, 666);
        Aggregation testRel = new Aggregation(testBox1,testBox2);
        testBox1.addRelationship(testRel);
        ArrayList<Relationship> testAL = testBox1.copyRelationships();
        assertTrue("Relationship Not Deleted",!testAL.isEmpty());
    }

    /**
     *
     * @throws Exception
     * Create two class boxes and one relationship,
     * then add the relationship to the class boxes.
     * Finally, delete the relationship from one of the class boxes.
     */
    @org.junit.Test
    public void testRemoveRelationship() throws Exception {
        new JFXPanel();
        ClassBox testBox1 = new ClassBox();
        ClassBox testBox2 = new ClassBox();
        Aggregation testRel = new Aggregation(testBox1,testBox2);
        testBox1.addRelationship(testRel);
        testBox1.removeRelationship(testRel);
        ArrayList<Relationship> testAL = testBox1.copyRelationships();
        assertTrue("Relationship Not Deleted",testAL.isEmpty());

    }

    /**
     *
     * @throws Exception
     * Create two class boxes and two relationships,
     * then add the relationships to the class boxes.
     * Then delete the relationships from one
     * box using removeAllRelationships() function
     */
    @org.junit.Test
    public void testRemoveAllRelationships() throws Exception {
        new JFXPanel();
        ClassBox testBox1 = new ClassBox();
        ClassBox testBox2 = new ClassBox();
        Aggregation testRel1 = new Aggregation(testBox1,testBox2);
        Aggregation testRel2 = new Aggregation(testBox1,testBox2);
        testBox1.addRelationship(testRel1);
        testBox1.addRelationship(testRel2);
        testBox1.removeRelationship(testRel1);
        testBox1.removeRelationship(testRel2);
        ArrayList<Relationship> testAL = testBox1.copyRelationships();
        assertTrue("Relationship Not Deleted",testAL.isEmpty());
    }
}