package CommandObjects;

import BoxObjects.ClassBox;
import Relationships.Aggregation;
import Relationships.Relationship;
import View.CanvasManager;
import javafx.embed.swing.JFXPanel;
import javafx.scene.layout.Pane;
import junit.framework.TestCase;
import ThreadingRule.JavaFXThreadingRule;
import CommandObjects.Utility;
import org.junit.Rule;

import java.util.ArrayList;

/**
 * Created by UFOF on 4/30/15.
 */
public class UtilityTest extends TestCase {
    @Rule
    JavaFXThreadingRule rule = new JavaFXThreadingRule();

    /**
     * *
     * @throws Exception
     * Create a canvas manager,
     * two classboxes, one relationship,
     * and one utility object and use the utility
     * object to create a relationship
     * between the two classboxes.
     */
    @org.junit.Test
    public void testCreateRelationship() throws Exception {
        new JFXPanel();
        Utility testUtl = new Utility();
        Pane testPane = new Pane();
        CanvasManager cm = new CanvasManager();
        cm.setCanvas(testPane);
        ClassBox testBox1 = new ClassBox();
        ClassBox testBox2 = new ClassBox();
        testBox1.setCanvasManager(cm);
        testBox2.setCanvasManager(cm);
        Aggregation testRel = new Aggregation(testBox1, testBox2);
        cm.setCanvas(testPane);
        assertTrue(cm.getCanvas()==testPane);
        testUtl.createRelationship(testRel);
        ArrayList<Relationship> testAL = testBox1.copyRelationships();
        assertTrue("Relationship Not Deleted",!testAL.isEmpty());
    }

    /**
     *
     * @throws Exception
     * Create a canvas manager,
     * two classboxes, one relationship,
     * and one utility object and use the utility
     * object to create a relationship
     * between the two classboxes.
     * Then delete the relationship
     * using the utility object.
     */
    @org.junit.Test
    public void testDestroyRelationship() throws Exception {
        new JFXPanel();
        Utility testUtl = new Utility();
        Pane testPane = new Pane();
        CanvasManager cm = new CanvasManager();
        cm.setCanvas(testPane);
        ClassBox testBox1 = new ClassBox();
        ClassBox testBox2 = new ClassBox();
        testBox1.setCanvasManager(cm);
        testBox2.setCanvasManager(cm);
        Aggregation testRel = new Aggregation(testBox1, testBox2);
        assertTrue(cm.getCanvas()==testPane);
        testUtl.createRelationship(testRel);
        testUtl.destroyRelationship(testRel);
        ArrayList<Relationship> testAL = testBox1.copyRelationships();
        assertTrue("Relationship Not Deleted",testAL.isEmpty());
    }

}