package View;

import BoxObjects.ClassBox;
import javafx.embed.swing.JFXPanel;
import junit.framework.TestCase;
import ThreadingRule.JavaFXThreadingRule;
import View.CanvasManager;
import org.junit.Rule;
import javafx.scene.layout.Pane;

/**
 * Created by UFOF on 4/30/15.
 */
public class CanvasManagerTest extends TestCase {
    @Rule
    JavaFXThreadingRule rule = new JavaFXThreadingRule();

    /**
     *
     * @throws Exception
     * Create a canvasManager and a tooltype
     * then set the canvas manager’s current tool to the tooltype.
     * Compare canvasManger currentTool to tooltype.
     */
    @org.junit.Test
    public void testSetCurrentTool() throws Exception {
        new JFXPanel();
        Pane testPane = new Pane();
        CanvasManager cm = new CanvasManager();
        cm.setCanvas(testPane);
        ClassBox testBox = new ClassBox();
        testBox.setCanvasManager(cm);
        ToolType testTool = ToolType.DELETE;
        cm.setCurrentTool(testTool);
        assertTrue("Tool Not Set", testTool == cm.getCurrentTool());
    }

    /**
     *
     * @throws Exception
     * Create a canvasManager and add an UML object
     * to the canvasManager then check
     * to see if the canvas has a child
     */
    @org.junit.Test
    public void testAddUMLObject() throws Exception {
        new JFXPanel();
        Pane testPane = new Pane();
        CanvasManager cm = new CanvasManager();
        cm.setCanvas(testPane);
        cm.addUMLObject(4,5);
        assertTrue("Not Added",!cm.getCanvas().getChildren().isEmpty());
    }

    /**
     *
     * @throws Exception
     * Create a fileManager and a canvasManager
     * then set the fileManager as the canvasManager’s fileManager.
     * Compare canvasManager.getFileManager() to created fileManager.
     */
    @org.junit.Test
    public void testSetFileManager() throws Exception {
        new JFXPanel();
        FileManager fm = new FileManager();
        CanvasManager cm = new CanvasManager();
        cm.setFileManager(fm);
        assertTrue("File Manager Not Set",fm == cm.getFileManager());

    }
}