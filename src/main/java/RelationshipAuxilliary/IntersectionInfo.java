package RelationshipAuxilliary;

/**
 * This is a simple data type that holds public data for the sole reason of easy and accurate access to
 * the intersection information between a class box and relationship line. This also holds information about
 * the angle of the line at the class box and the location for the cardinality.
 */
public class IntersectionInfo
{
	public double xIntersect, yIntersect, cardinalityX, cardinalityY, angle;

	/**
	 * Constructs this object to initialize all fields with given information.
	 *
	 * @param xIntersect The x coordinate of the intersection of the box and line.
	 * @param yIntersect The y coordinate of the intersection of the box and line.
	 * @param cardinalityX The x coordinate of cardinality text location.
	 * @param cardinalityY The y coordinate of cardinality text location.
	 * @param angleInDegrees The angle that this line makes, counter-clockwise from the horizontal.
	 */
	public IntersectionInfo ( double xIntersect, double yIntersect,
	                          double cardinalityX, double cardinalityY, double angleInDegrees )
	{
		this.xIntersect = xIntersect;
		this.yIntersect = yIntersect;
		this.cardinalityX = cardinalityX;
		this.cardinalityY = cardinalityY;
		this.angle = angleInDegrees;
	}
}
