package RelationshipAuxilliary;

/**
 * This is a simple data structure that is made to hold text field orientation information of a relationship
 * description. The goal is to have intuitive access to the information, and allow parameter passing as a group.
 */
public class MiddleTextInfo
{
	/** The x coordinate of the center of the text field. */
	public double centerX;
	/** The y coordinate of the center of the text field. */
	public double centerY;
	/** The angle that the text field needs to be rotated by to achieve alignment with the middleLine. */
	public double rotationAngleInDegrees;

	/**
	 * Constructs this object to initialize all fields with given information.
	 *
	 * @param centerX The x coordinate of where the relationship text will be placed.
	 * @param centerY The y coordinate of where the relationship text will be placed.
	 * @param rotationAngleInDegrees The angle of the conventional middle line of the <code>JointedLine</code>.
	 */
	public MiddleTextInfo ( double centerX, double centerY, double rotationAngleInDegrees )
	{
		this.centerX = centerX;
		this.centerY = centerY;
		this.rotationAngleInDegrees = rotationAngleInDegrees;
	}
}
