package RelationshipAuxilliary;

import CommandObjects.EditRelationshipText;
import View.CanvasManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

/**
 * This object is for use as text on a relationship line. That includes text for cardinality and a label.
 * These text fields only appear if they have content or the text editing tool is selected and the mouse is either
 * over the field or the line that it belongs to.
 */
public class RelationshipTextField extends TextField
{
	boolean isUsed;
	boolean mouseIsIn;
	CanvasManager canvasManager;
	/** Used by the command object so that changing the text as an undo/redo operation is not listened to. */
	public boolean ignoreChange = false;

	/**
	 * Makes the field opaque when the mouse enters.
	 */
	EventHandler< MouseEvent > onEnter = new EventHandler< MouseEvent > ( )
	{
		@Override
		public void handle ( MouseEvent event )
		{
			if ( !isUsed )
			{
				setStyle ( "-fx-opacity: 1;" );
			}
			mouseIsIn = true;
		}
	};

	/**
	 * Makes the field transparent when the mouse exits, unless it has content.
	 */
	EventHandler< MouseEvent > onExit = new EventHandler< MouseEvent > ( )
	{
		@Override
		public void handle ( MouseEvent event )
		{
			if ( !isUsed )
			{
				setStyle ( "-fx-opacity: 0;" );
			}
			mouseIsIn = false;
		}
	};

	/**
	 * This constructor mimics the <code>TextField</code> constructor with <code>String</code> argument. This creates
	 * a default text field with initial text, and then modifies the text field to differentiate this from a
	 * <code>TextField</code>.
	 *
	 * @param text Initial text for the <code>RelationshipTextField</code>.
	 */
	public RelationshipTextField ( CanvasManager canvasManager, String text )
	{
		super ( text );
		isUsed = true;
		this.canvasManager = canvasManager;
		initRelTextField ( );
	}

	/**
	 * This constructor mimics the <code>TextField</code> no-argument constructor. This creates
	 * a default text field with no text, and then modifies the text field to differentiate this from a
	 * <code>TextField</code>.
	 */
	public RelationshipTextField ( CanvasManager canvasManager )
	{
		isUsed = false;
		this.canvasManager = canvasManager;
		setStyle ( "-fx-opacity: 0;" );
		initRelTextField ( );
	}

	/**
	 * Initializes this <code>RelationshipTextField</code>; this method is necessary for both constructors.
	 */
	private void initRelTextField ( )
	{
		// TODO It would be cool if the background was transparent.
		setOnMouseEntered ( onEnter );
		setOnMouseExited ( onExit );
		refreshVisibility ( );
		refreshSize ( );

		textProperty ( ).addListener ( new ChangeListener< String > ( )
		{
			@Override
			public void changed ( ObservableValue< ? extends String > observable, String oldValue, String newValue )
			{
				if ( !ignoreChange )
				{
					isUsed = newValue.length ( ) != 0;
					refreshVisibility ( );
					refreshSize ( );

					EditRelationshipText edit = new EditRelationshipText ( getThis ( ), oldValue, newValue );
					canvasManager.commandAcceptor.acceptCommand ( edit );
				}
			}
		} );
	}

	/**
	 * Returns this instance:  for use in listeners or handlers.
	 *
	 * @return This instance.
	 */
	private RelationshipTextField getThis ( )
	{
		return this;
	}

	/**
	 * Sets the boxes visibility. This does not allow a used box to disappear.
	 *
	 * @param isVisible <code>true</code> to attempt to make box visible, <code>false</code> to make box invisible.
	 */
	public void setBoxVisible ( boolean isVisible )
	{
		if ( isVisible )
		{
			setStyle ( "-fx-opacity: 1;" );
		} else
		{
			if ( !isUsed )
			{
				setStyle ( "-fx-opacity: 0;" );
			}
		}
	}

	/**
	 * Re-evaluates the text box to adjust visibility based on if this box is used.
	 */
	public void refreshVisibility ( )
	{
		if ( isUsed )
		{
			setStyle ( "-fx-opacity: 1;" );
		} else
		{
			if ( !mouseIsIn )
				setStyle ( "-fx-opacity: 0;" );
		}
	}

	/**
	 * This is an algorithm to calculate a size for the field and set the field width to that.
	 * <p/>
	 * Given that the algorithm depends only on text length, rather than font, characters, or otherwise, it is
	 * relatively inflexible.
	 */
	public void refreshSize ( )
	{
		Text tempText = new Text ( getText ( ) );
		double width = tempText.getLayoutBounds ( ).getWidth ( );
		setMinWidth ( width + 20 );
		setPrefWidth ( width + 20 );
	}
}
