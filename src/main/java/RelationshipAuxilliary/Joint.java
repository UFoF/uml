package RelationshipAuxilliary;

import CommandObjects.DestroyJoint;
import CommandObjects.MoveJoint;
import Relationships.Relationship;
import View.CanvasManager;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 * This is a circle that is characterized as a joint in a line, but is a joint between two accessible lines.
 * This class is not meant to stand apart from <code>JointedLine</code> in any way. It includes methods that
 * significantly alter the jointed line, and is therefore more a part of <code>JointedLine</code> than it is
 * an individual point.
 */
public class Joint extends Circle
{
	private static final double RADIUS = 10.0;
	private Line previous;
	private Line next;
	private CanvasManager canvasManager;
	private Relationship relationship;
	private double initialX, initialY;
	private double offsetX, offsetY;
	private boolean isBeingDragged;
	/*      DEBATE All labeled "Dragged" are the fix to the problem of the circle being made transparent whenever the
	        mouse "outruns" the circle. This can cause the circle to disappear during fast drags, or flash on slow
	        drags where the circle is grabbed near the edge.
	        The downside of my solution is that the mouse can "exit the circle" while being dragged if the circle
	        goes off of the edge. In this case, the circle is exited while dragging, the same cause as the previous
	        problem. My boolean test stops the circle from disappearing in the previous problem, but stops it from
	        disappearing in this problem.
	*/

	/**
	 * Event handler for when the mouse enters the circle. This sets the circle's color to black (opaque) so that the
	 * user can easily see where he is selecting the joint.
	 */
	private EventHandler< MouseEvent > onMouseEnter = new EventHandler< MouseEvent > ( )
	{
		@Override
		public void handle ( MouseEvent event )
		{
			setFill ( Color.BLACK );
		}
	};

	/**
	 * Event handler for the mouse exiting. This is used to set the circle to be transparent after the mouse leaves it.
	 */
	private EventHandler< MouseEvent > onMouseExit = new EventHandler< MouseEvent > ( )
	{
		@Override
		public void handle ( MouseEvent event )
		{
			if ( !isBeingDragged ) setFill ( Color.TRANSPARENT );
			//			setFill ( Color.TRANSPARENT ); // Insert this if not using "isBeingDragged."
		}
	};

	/**
	 * The event handler for mouse pressing. For dragging, it captures the offset of the circle's center from the mouse
	 * click location. This is important so that the circle knows where to be relative to the mouse throughout the
	 * drag.
	 * This handler also handles joint deletion - abstractly merging it with the point to its left or right.
	 */
	private EventHandler< MouseEvent > onMousePress = new EventHandler< MouseEvent > ( )
	{
		@Override
		public void handle ( MouseEvent event )
		{
			switch ( canvasManager.getCurrentTool ( ) )
			{
				case CREATE_JOINT:
				case DRAG:
					if ( !relationship.isRecursive )
					{
						event.consume ( );
						isBeingDragged = true;
						initialX = getCenterX ( );
						initialY = getCenterY ( );
						offsetX = getCenterX ( ) - event.getX ( );
						offsetY = getCenterY ( ) - event.getY ( );
					}
					break;
				case DELETE:
					event.consume ( );
					DestroyJoint d = new DestroyJoint ( getThis ( ) );
					canvasManager.commandAcceptor.acceptCommand ( d );
			}
		}
	};

	/**
	 * This is the event handler for mouse drags. This moves the joint, setting the circle's x and y positions.
	 * Notably, the joint is not allowed to move outside of the bounds of the canvas.
	 */
	private EventHandler< MouseEvent > onMouseDrag = new EventHandler< MouseEvent > ( )
	{
		@Override
		public void handle ( MouseEvent event )
		{
			switch ( canvasManager.getCurrentTool ( ) )
			{
				case CREATE_JOINT:
				case DRAG:
					if ( !relationship.isRecursive )
					{
						Pane canvas = canvasManager.getCanvas ( );
						double newX = event.getX ( ) + offsetX;
						double newY = event.getY ( ) + offsetY;

						// Set the x position.
						if ( newX - RADIUS < 0 )
						{
							setCenterX ( RADIUS );
						} else if ( newX + RADIUS > canvas.getWidth ( ) )
						{
							setCenterX ( canvas.getWidth ( ) - RADIUS );
						} else
						{
							setCenterX ( newX );
						}

						// Set the y position.
						if ( newY - RADIUS < 0 )
						{
							setCenterY ( RADIUS );
						} else if ( newY + RADIUS > canvas.getHeight ( ) )
						{
							setCenterY ( canvas.getHeight ( ) - RADIUS );
						} else
						{
							setCenterY ( newY );
						}

						event.consume ( );

						relationship.update ( );
					}
					break;
			}
		}
	};

	/**
	 * This is the event handler for the release of the mouse button. If implemented, it sets
	 * <code>isBeingDragged</code> to false, since releasing the mouse implies that the drag has ended. Also, it
	 * uses the command object interface to record the movement.
	 */
	EventHandler< MouseEvent > onMouseRelease = new EventHandler< MouseEvent > ( )
	{
		@Override
		public void handle ( MouseEvent event )
		{
			switch ( canvasManager.getCurrentTool ( ) )
			{
				case CREATE_JOINT:
				case DRAG:
					if ( !relationship.isRecursive )
					{
						isBeingDragged = false;
						double finalX = getThis ( ).getCenterX ( );
						double finalY = getThis ( ).getCenterY ( );
						MoveJoint m = new MoveJoint ( getThis ( ), initialX, initialY, finalX, finalY );
						canvasManager.commandAcceptor.acceptCommand ( m );

						if ( !contains ( event.getX ( ), event.getY ( ) ) )
						{
							setFill ( Color.TRANSPARENT );
						}
					}
					break;
			}
		}
	};

	/**
	 * Constructs a default <code>Joint</code>, which is a set of event handlers and default variables. This is only
	 * to be used in saving and loading, and only called on by the JVM.
	 */
	public Joint ( )
	{
		// Do nothing.
	}

	/**
	 * Constructs a <code>Joint</code> to be a joint between the two lines. Note that this does not bind the lines,
	 * as that has its own method.
	 *
	 * @param previous The first line for the joint.
	 * @param next The second line for the joint.
	 * @param initialXPosition The initial x coordinate for the joint's placement.
	 * @param initialYPosition The initial y coordinate for the joint's placement.
	 */
	public Joint ( Line previous, Line next, double initialXPosition, double initialYPosition, Relationship relationship )
	{
		this.previous = previous;
		this.next = next;
		canvasManager = relationship.canvasManager;

		setCenterX ( initialXPosition );
		setCenterY ( initialYPosition );

		this.relationship = relationship;

		setStroke ( Color.TRANSPARENT );
		setStrokeWidth ( 0 );
		setRadius ( RADIUS );
		setFill ( Color.TRANSPARENT );

		setOnMouseEntered ( onMouseEnter );
		setOnMouseExited ( onMouseExit );
		setOnMousePressed ( onMousePress );
		setOnMouseDragged ( onMouseDrag );
		setOnMouseReleased ( onMouseRelease );
	}

	/**
	 * Binds the two lines held by the joint to the joint, such the the previous line ends at the joint and the next
	 * line starts at the joint.
	 */
	public void bindLines ( )
	{
		previous.endXProperty ( ).bind ( centerXProperty ( ) );
		previous.endYProperty ( ).bind ( centerYProperty ( ) );
		next.startXProperty ( ).bind ( centerXProperty ( ) );
		next.startYProperty ( ).bind ( centerYProperty ( ) );
	}

	/**
	 * Sets the previous line to this joint. This is used for making a new joint
	 * and line or removing a joint and line.
	 */
	public void setPrevious ( Line previous )
	{
		this.previous = previous;
	}

	/**
	 * Returns the line that is adjacent to this  joint towards the child class box. This is used for
	 * command object algorithms.
	 */
	public Line getPrevious ( )
	{
		return previous;
	}

	/** Returns this instance. Used to give the event handlers access to this joint. */
	private Joint getThis ( )
	{
		return this;
	}

	/**
	 * Returns the relationship of this joint.
	 *
	 * @return A reference to this object.
	 */
	public Relationship getRelationship ( )
	{
		return relationship;
	}
}
