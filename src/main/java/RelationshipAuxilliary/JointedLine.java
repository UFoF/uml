package RelationshipAuxilliary;

import BoxObjects.ClassBox;
import CommandObjects.CreateJoint;
import CommandObjects.DestroyRelationship;
import Relationships.Relationship;
import View.CanvasManager;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

import java.util.LinkedList;

/**
 * This is meant to be similar to a <code>Polyline</code> except that it has <code>Joint</code> objects between
 * lines. This class is specialized, however, in that it requires a <code>Relationship</code> object, and binds to the
 * class boxes.
 */
public class JointedLine extends Group
{
	// TODO TEST Lines magnitude should be equal to joints +1.
	/** Shows debugging information as line color. */
	private static final boolean COLOR_DEBUG = CanvasManager.DEBUG;
	/** Holds the lines, in order. */
	private LinkedList< Line > lines;
	/** Holds the joints, in order. */
	private LinkedList< Joint > joints; // Needed for assigning lines to existing joints.
	/**
	 * Holds the <code>int</code> index of the conventional middle line. Convention for this class is
	 * that the middle line is the middle line of a list of odd parity, and the previous of the two
	 * middle pieces of a list of even parity.
	 */
	private int middleLineIndex = 0;
	private CanvasManager canvasManager;
	Relationship relationship;

	/** TODO */
	private EventHandler< MouseEvent > onMousePress = new EventHandler< MouseEvent > ( )
	{
		@Override
		public void handle ( MouseEvent event )
		{
			switch ( relationship.canvasManager.getCurrentTool ( ) )
			{
				case DELETE:

					DestroyRelationship d = new DestroyRelationship ( relationship );
					relationship.canvasManager.commandAcceptor.acceptCommand ( d );
					break;
				case CREATE_JOINT:
					// TODO Command object.
					Line intersectedLine = (Line) event.getSource ( );
					double x = event.getX ( );
					double y = event.getY ( );
					CreateJoint c = new CreateJoint ( getThis ( ), intersectedLine, x, y );
					canvasManager.commandAcceptor.acceptCommand ( c );
			}

			if ( COLOR_DEBUG )
			{
				//				lines.get ( middleLineIndex ).setStroke ( Color.BLUE );
				for ( int i = 0; i < lines.size ( ); i++ )
				{
					if ( i != middleLineIndex )
					{
						lines.get ( i ).setStroke ( Color.GREEN );
					}
				}
				lines.get ( middleLineIndex ).setStroke ( Color.BLUE );
			}

			event.consume ( );
		}
	};

	//	/**
	//	 * Constructs a <code>JointedLine</code> between the child and parent of the relationship so that there are no
	//	 * joints.
	//	 *
	//	 * @param relationship The relationship that this jointed line is for.
	//	 */
	//	public JointedLine ( Relationship relationship )
	//	{
	//		this.relationship = relationship;
	//		canvasManager = relationship.canvasManager;
	//
	//		Line firstLine = new Line ( );
	//		relationship.formatLine ( firstLine );
	//
	//		lines = new LinkedList< Line > ( );
	//		lines.add ( firstLine );
	//		getChildren ( ).add ( firstLine );
	//
	//		// Tie the line to the class boxes.
	//		ClassBox tempChild = relationship.getChildReference ( );
	//		ClassBox tempParent = relationship.getParentReference ( );
	//		firstLine.startXProperty ( ).bind ( tempChild.xProperty ( ).add ( tempChild.widthProperty ( ).divide ( 2 ) ) );
	//		firstLine.startYProperty ( ).bind ( tempChild.yProperty ( ).add ( tempChild.heightProperty ( ).divide ( 2 ) ) );
	//		firstLine.endXProperty ( ).bind ( tempParent.xProperty ( ).add ( tempParent.widthProperty ( ).divide ( 2 ) ) );
	//		firstLine.endYProperty ( ).bind ( tempParent.yProperty ( ).add ( tempParent.heightProperty ( ).divide ( 2 ) ) );
	//
	//		firstLine.setOnMousePressed ( onMousePress );
	//
	//		joints = new LinkedList< Joint > ( );
	//	}

	/**
	 * This constructor is strictly for saving and loading, and thus should be used by none other than the JVM.
	 */
	public JointedLine ( )
	{
		// Do nothing.
	}

	/**
	 * Constructs a <code>JointedLine</code> from the child to parent of the given relationship. If there are no
	 * coordinates given, a single line is drawn. If the coordinates are given, and are even in quantity, then joints
	 * will be placed at the implied (x,y) coordinates. The coordinates are meant to be x-y pairs, but this is not
	 * forced so that the constructor is simple to use.
	 * <p/>
	 * If there is an odd number of coordinates, an <code>IllegalArgumentException</code> will be thrown.
	 *
	 * @param relationship The relationship that this jointed line is a part of.
	 * @param coordinates The list of two dimensional coordinates, listed as x1, y1, x2, y2, ... xn, yn.
	 *
	 * @throws IllegalArgumentException in the case of an odd number of coordinates.
	 */
	public JointedLine ( Relationship relationship, double... coordinates ) throws IllegalArgumentException
	{
		this.relationship = relationship;
		canvasManager = relationship.canvasManager;

		Line firstLine = new Line ( );
		relationship.formatLine ( firstLine );

		lines = new LinkedList< Line > ( );
		lines.add ( firstLine );
		getChildren ( ).add ( firstLine );

		// Tie the line to the class boxes.
		ClassBox tempChild = relationship.getChildReference ( );
		ClassBox tempParent = relationship.getParentReference ( );
		firstLine.startXProperty ( ).bind ( tempChild.translateXProperty ( ).add ( tempChild.widthProperty ( ).divide ( 2 ) ) );
		firstLine.startYProperty ( ).bind ( tempChild.translateYProperty ( ).add ( tempChild.heightProperty ( ).divide ( 2 ) ) );
		firstLine.endXProperty ( ).bind ( tempParent.translateXProperty ( ).add ( tempParent.widthProperty ( ).divide ( 2 ) ) );
		firstLine.endYProperty ( ).bind ( tempParent.translateYProperty ( ).add ( tempParent.heightProperty ( ).divide ( 2 ) ) );

		firstLine.setOnMousePressed ( onMousePress );

		joints = new LinkedList< Joint > ( );

		if ( coordinates.length % 2 == 0 )
		{
			Joint joint;
			ClassBox child;
			for ( int i = 0; i < coordinates.length; i += 2 )
			{
				if ( relationship.isRecursive )
				{
					joint = addJoint ( lines.getLast ( ), coordinates[ i ], coordinates[ i + 1 ] );
					child = relationship.getChildReference ( );
					double childX = child.getTranslateX ( );
					double childY = child.getTranslateY ( );
					joint.centerXProperty ( ).bind ( child.translateXProperty ( ).add ( coordinates[ i ] - childX ) );
					joint.centerYProperty ( ).bind ( child.translateYProperty ( ).add ( coordinates[ i + 1 ] - childY ) );
				}
			}
		} else
		{
			throw new IllegalArgumentException ( );
		}
	}

	/**
	 * Sets the x coordinate of the start of the jointed line.
	 *
	 * @param newStartX The desired x coordinate of the start of the jointed line.
	 */
	public void setStartX ( double newStartX )
	{
		lines.getFirst ( ).setStartX ( newStartX );
	}

	/**
	 * Sets the y coordinate of the start of the jointed line.
	 *
	 * @param newStartY The desired y coordinate of the start of the jointed line.
	 */
	public void setStartY ( double newStartY )
	{
		lines.getFirst ( ).setStartY ( newStartY );
	}

	/**
	 * Sets the x coordinate of the end of the jointed line.
	 *
	 * @param newEndX The desired x coordinate of the end of the jointed line.
	 */
	public void setEndX ( double newEndX )
	{
		lines.getLast ( ).setEndY ( newEndX );
	}

	/**
	 * Sets the y coordinate of the end of the jointed line.
	 *
	 * @param newEndY The desired y coordinate of the end of the jointed line.
	 */
	public void setEndY ( double newEndY )
	{
		lines.getLast ( ).setEndY ( newEndY );
	}

	/**
	 * Returns a copy of the starting line of the jointed line. This is to prevent modification of the jointed line.
	 * It also prevents misuse based on misunderstanding, as the first line reference might not refer to the first
	 * line after it is moved due to modifications of the jointed line, and it may become null.
	 */
	public Line getStart ( )
	{
		Line first = lines.getFirst ( );
		return new Line ( first.getStartX ( ), first.getStartY ( ), first.getEndX ( ), first.getEndY ( ) );
	}

	/**
	 * Returns a copy of the starting line of the jointed line. This is to prevent modification of the jointed line.
	 * It also prevents misuse based on misunderstanding, as the first line reference might not refer to the first
	 * line after it is moved due to modifications of the jointed line, and it may become null.
	 *
	 * @return A copy of the line that is considered to be the conventional middle segment.
	 */
	public Line getMiddle ( )
	{
		Line middle = lines.get ( middleLineIndex );
		return new Line ( middle.getStartX ( ), middle.getStartY ( ), middle.getEndX ( ), middle.getEndY ( ) );
	}

	/**
	 * Returns a copy of the ending line of the jointed line. This is to prevent modification of the jointed line.
	 * It also prevents misuse based on misunderstanding, as the end line reference might not refer to the end
	 * line after it is moved due to modifications of the jointed line, and it may become null.
	 */
	public Line getEnd ( )
	{
		Line end = lines.getLast ( );
		return new Line ( end.getStartX ( ), end.getStartY ( ), end.getEndX ( ), end.getEndY ( ) );
	}

	/**
	 * Takes the line that is to receive a joint, adds a new line, and adjusts both to be contiguous at the joint.
	 * Also initializes the joint, and binds the lines to it.
	 * <p/>
	 * Returning the joint gives access to the created joint. This is taken advantage of in the command object
	 * interface.
	 *
	 * @param line
	 * @param intersectX
	 * @param intersectY
	 *
	 * @return The joint that was just created.
	 */
	public Joint addJoint ( Line line, double intersectX, double intersectY )
	{
		int preInsertionIndex = lines.indexOf ( line );

		// Add the new line.
		Line newLine = new Line ( );
		//		newLine.setOnMousePressed ( onMousePress ); This doesn't work here - interesting. - Eric.
		//      Maybe the binding or collection or group has something to do with that.
		lines.add ( preInsertionIndex + 1, newLine ); // Adds line to the right of the joint.
		getChildren ( ).add ( newLine );
		relationship.formatLine ( newLine );

		// DEBUG Line colors.
		if ( COLOR_DEBUG )
		{
			line.setStroke ( Color.RED );
			newLine.setStroke ( Color.GREEN );
		}

		// Add the joint.
		Joint newJoint = new Joint ( line, newLine, intersectX, intersectY, relationship );
		joints.add ( preInsertionIndex, newJoint );
		getChildren ( ).add ( newJoint );
		if ( relationship.isRecursive )
		{
			ClassBox child = relationship.getChildReference ( );
			double offsetX = intersectX - child.getTranslateX ( );
			newJoint.centerXProperty ( ).bind ( child.translateXProperty ( ).add ( offsetX ) );
			double offsetY = intersectY - child.getTranslateY ( );
			newJoint.centerYProperty ( ).bind ( child.translateYProperty ( ).add ( offsetY ) );
		}
		newJoint.bindLines ( );

		// Move the pointer to the middle, if necessary.
		// NOTE This only works after the addition of the line to the linked list in this method. Otherwise, test for even.
		if ( lines.size ( ) % 2 == 1 ) ++middleLineIndex;

		// Force nice layering.
		newJoint.toBack ( );
		newLine.toBack ( );
		line.toBack ( );

		if ( lines.getLast ( ) == newLine )
		{
			ClassBox tempParent = relationship.getParentReference ( );
			// Next two lines: x + 1/2 * width and y + 1/2 * height.
			newLine.endXProperty ( ).bind ( tempParent.translateXProperty ( ).add ( tempParent.widthProperty ( ).divide ( 2 ) ) );
			newLine.endYProperty ( ).bind ( tempParent.translateYProperty ( ).add ( tempParent.heightProperty ( ).divide ( 2 ) ) );
		} else
		{
			Joint jointRef = joints.get ( preInsertionIndex + 1 );
			jointRef.setPrevious ( newLine );
			jointRef.bindLines ( );
		}
		newLine.setOnMousePressed ( onMousePress ); // NOTE This seems to have to be at the end.

		return newJoint;
	}

	/**
	 * Removes the specified joint from the line and the leftover line, and rebinds the remaining lines depending
	 * on what they have to bind to. Joints and lines are also removed from this group. Finally, the relationship
	 * is updated so that the arrowhead of the relationship is relevant to the new shape of the jointed line.
	 *
	 * @param joint The joint to remove from this jointed line.
	 */
	public void removeJoint ( Joint joint )
	{
		int indexOfJoint = joints.indexOf ( joint );

		// TODO TEST here and at the end that the size of joints is 1 less than that of lines.

		getChildren ( ).remove ( joint );
		joints.remove ( indexOfJoint );
		getChildren ( ).remove ( lines.get ( indexOfJoint + 1 ) ); // Deletes the line after the joint.
		lines.remove ( indexOfJoint + 1 );

		if ( indexOfJoint == joints.size ( ) ) // Met only when it was the last joint that was removed.
		{
			Line lineRef = lines.getLast ( );
			ClassBox parentRef = relationship.getParentReference ( );
			// Next two lines: x + 1/2 * width and y + 1/2 * height.
			lineRef.endXProperty ( ).bind ( parentRef.translateXProperty ( ).add ( parentRef.widthProperty ( ).divide ( 2 ) ) );
			lineRef.endYProperty ( ).bind ( parentRef.translateYProperty ( ).add ( parentRef.heightProperty ( ).divide ( 2 ) ) );

			// Make the layering work well.
			lineRef.toBack ( );

			relationship.update ( ); // Updates the arrowhead.
		} else
		{
			Line lineRef = lines.get ( indexOfJoint );
			Joint jointRef = joints.get ( indexOfJoint );
			jointRef.setPrevious ( lineRef );
			jointRef.bindLines ( );

			// Make the layering work well.
			lineRef.toBack ( );
		}

		// Move the pointer to the conventional middle, if necessary.
		// NOTE This only works after the removal of the line to the linked list in this method. Otherwise, test for odd.
		if ( lines.size ( ) % 2 == 0 )
		{
			--middleLineIndex;
		}
	}

	/** Adds this jointed line to the canvas of its canvas manager. */
	public void addSelf ( )
	{
		canvasManager.getCanvas ( ).getChildren ( ).add ( this );
	}

	/** Removes this jointed line from the canvas of its canvas manager. */
	public void removeSelf ( )
	{
		canvasManager.getCanvas ( ).getChildren ( ).remove ( this );
	}

	/**
	 * Returns this instance. Used to give the event handlers access to this joint.
	 *
	 * @return A reference to this object.
	 */
	private JointedLine getThis ( )
	{
		return this;
	}
}
