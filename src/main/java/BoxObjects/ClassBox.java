package BoxObjects;

import CommandObjects.EditClassBoxTextArea;
import Relationships.Relationship;
import View.CanvasManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Cursor;
import javafx.scene.control.TextArea;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.util.ArrayList;

/**
 * This represents a graphical UML class diagram class box.
 * <p/>
 * <code>Relationship</code> references are stored in an <code>ArrayList</code> if they are connected to this class
 * box,
 * and this class box calls on the relationships to modify themselves as necessary.
 */
public class ClassBox extends VBox
{
	//initialize rectangle with default height and width
	public final static double DEFAULT_WIDTH = 125;
	public final static double DEFAULT_HEIGHT = 175;
	private ArrayList< Relationship > relationships;
	private CanvasManager canvasManager;

	private TextArea className = new TextArea ( );
	private TextArea classAttributes = new TextArea ( );
	private TextArea classOperations = new TextArea ( );
	private TextArea[] fields = new TextArea[ 3 ];

	private double oldHeight = 0;
	/**
	 * Indicates that the text areas should ignore the undo and redo for the change.
	 */
	public boolean ignoreChange;

	/**
	 * Constructs an empty <code>ClassBox</code>.
	 */
	public ClassBox ( )
	{
		initClassBox ( );
	}

	/**
	 * Constructs a <code>ClassBox</code> at the given location with default width and height.
	 * <p>
	 * Note that this is different from the standard two-<code>double</code> <code>Rectangle</code> constructor in that
	 * these
	 * <code>double</code> arguments are for position rather than dimensions.
	 * <p/>
	 * Dimensions are set to the <code>DEFAULT_WIDTH</code> and <code>DEFAULT_HEIGHT</code> of this class.
	 *
	 * @param x The desired x-coordinate.
	 * @param y The desired y-coordinate.
	 */
	public ClassBox ( double x, double y )
	{
		super ( );
		this.setTranslateX ( x );
		this.setTranslateY ( y );
		this.setWidth ( DEFAULT_HEIGHT );
		this.setHeight ( DEFAULT_WIDTH );
		initClassBox ( );
	}

	/**
	 * Constructs a <code>ClassBox</code> at a given location with given dimensions.
	 *
	 * @param x The desired x-coordinate.
	 * @param y The desired y-coordinate.
	 * @param width The desired horizontal span.
	 * @param height The desired vertical span.
	 */
	public ClassBox ( double x, double y, double width, double height )
	{
		super ( );
		this.setTranslateX ( x );
		this.setTranslateY ( y );
		this.setHeight ( height );
		this.setWidth ( width );
		initClassBox ( );
	}

	/**
	 * Initializes the <code>ClassBox</code> with set fill and stroke colors, as well as the default cursor.
	 * The relationships array is also initialized.
	 * <p/>
	 * The purpose of this method is to eliminate repeat code and implement information hiding for use in all
	 * constructors.
	 */
	private void initClassBox ( )
	{
		relationships = new ArrayList< Relationship > ( );
		setCursor ( Cursor.OPEN_HAND ); // TODO Do we ALWAYS want the open hand? - Eric
		this.setBorder ( new Border ( new BorderStroke ( Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii ( 3 ), new BorderWidths ( 3 ) ) ) );

		populateClassBox ( );
	}

	/**
	 * Disallows the <code>TextArea</code> objects of this object from receiving mouse events.
	 */
	public void disableTextArea ( )
	{
		for ( TextArea t : fields )
		{
			t.setMouseTransparent ( true );
		}
	}

	/**
	 * Allows the <code>TextArea</code> objects of this object to receive mouse events.
	 */
	public void enableTextArea ( )
	{
		for ( TextArea t : fields )
		{
			t.setMouseTransparent ( false );
		}
	}

	/**
	 * Initializes text areas
	 */
	private void populateClassBox ( )
	{
		fields[ 0 ] = className;
		fields[ 1 ] = classAttributes;
		fields[ 2 ] = classOperations;

		className.setPromptText ( "Class Name" );
		classAttributes.setPromptText ( "Class Variables" );
		classOperations.setPromptText ( "Class Functions" );
		resizeTextAreas ( );

		for ( TextArea t : fields )
		{
			t.setMinWidth ( DEFAULT_WIDTH );
			t.setMinHeight ( DEFAULT_HEIGHT / 3 );
			t.setPrefWidth ( this.getWidth ( ) );
			t.setPrefHeight ( this.getHeight ( ) / 3 );
			t.setMouseTransparent ( true );
			t.setBorder ( new Border ( new BorderStroke ( Color.GRAY, BorderStrokeStyle.SOLID, null, new BorderWidths ( 2 ) ) ) );
			this.getChildren ( ).add ( t );
		}
	}

	/**
	 * add listeners to text areas to resize them
	 */
	private void resizeTextAreas ( )
	{
		className.textProperty ( ).addListener ( new ChangeListener< String > ( )
		{
			@Override
			public void changed ( ObservableValue< ? extends String > observable, String oldValue, String newValue )
			{
				if ( !ignoreChange )
				{
					EditClassBoxTextArea e = new EditClassBoxTextArea ( getThis ( ), className, oldValue, newValue );
					canvasManager.commandAcceptor.acceptCommand ( e );
				}
				Text tempText = new Text ( className.getText ( ) );
				double width = tempText.getLayoutBounds ( ).getWidth ( );
				double height = tempText.getLayoutBounds ( ).getHeight ( );
//								className.setPrefWidth ( width + 40 );
				className.setMinWidth ( Math.max ( width, DEFAULT_WIDTH ) ); // TODO Sloppy way to keep box from shrinking
//								className.setPrefHeight ( height + 10 );
				className.setPrefHeight ( Math.max ( height, DEFAULT_HEIGHT / 3 ) );
				updateRelationships ( );
			}
		} );

		classAttributes.textProperty ( ).addListener ( new ChangeListener< String > ( )
		{
			@Override
			public void changed ( ObservableValue< ? extends String > observable, String oldValue, String newValue )
			{
				if ( !ignoreChange )
				{
					EditClassBoxTextArea e = new EditClassBoxTextArea ( getThis ( ), classAttributes, oldValue, newValue );
					canvasManager.commandAcceptor.acceptCommand ( e );
				}
				Text tempText = new Text ( classAttributes.getText ( ) );
				double width = tempText.getLayoutBounds ( ).getWidth ( );
				double height = tempText.getLayoutBounds ( ).getHeight ( );
				classAttributes.setPrefWidth ( width + 40 );
				classAttributes.setPrefHeight ( height + 10 );
				updateRelationships ( );
			}
		} );

		classOperations.textProperty ( ).addListener ( new ChangeListener< String > ( )
		{
			@Override
			public void changed ( ObservableValue< ? extends String > observable, String oldValue, String newValue )
			{
				if ( !ignoreChange )
				{
					EditClassBoxTextArea e = new EditClassBoxTextArea ( getThis ( ), classOperations, oldValue, newValue );
					canvasManager.commandAcceptor.acceptCommand ( e );
				}
				Text tempText = new Text ( classOperations.getText ( ) );
				double width = tempText.getLayoutBounds ( ).getWidth ( );
				double height = tempText.getLayoutBounds ( ).getHeight ( );
				classOperations.setPrefWidth ( width + 40 );
				classOperations.setPrefHeight ( height + 10 );
				updateRelationships ( );
			}
		} );

		//		className.setOnKeyPressed(new EventHandler<KeyEvent>() {
		//			@Override
		//			public void handle(KeyEvent event) {
		//				if (event.getCode() == KeyCode.ENTER) {
		//					className.setPrefHeight(className.getHeight() + className.getFont().getSize());
		//					//className.setPrefRowCount(className.getPrefRowCount() + 1);
		//					updateRelationships();
		//				}
		//
		//				if(event.getCode() == KeyCode.BACK_SPACE) {
		//					className.setMaxHeight ( className.getPrefRowCount ( ) * 12 );
		//				}
		//			}
		//		});
		//
		//		classAttributes.setOnKeyPressed(new EventHandler<KeyEvent>() {
		//			@Override
		//			public void handle(KeyEvent event) {
		//				if (event.getCode() == KeyCode.ENTER) {
		//					classAttributes.setPrefHeight(classAttributes.getHeight() + 15);
		//					classAttributes.setPrefRowCount(classAttributes.getPrefRowCount() + 1);
		//					updateRelationships();
		//				}
		//			}
		//		});
		//
		//		classOperations.setOnKeyPressed(new EventHandler<KeyEvent>() {
		//			@Override
		//			public void handle(KeyEvent event) {
		//				if (event.getCode() == KeyCode.ENTER) {
		//					classOperations.setPrefHeight(classOperations.getHeight() + 15);
		//					classOperations.setPrefRowCount(classOperations.getPrefRowCount() + 1);
		//					updateRelationships();
		//				}
		//			}
		//		});
	}

	/**
	 * Sets the canvas manager for this class box.
	 *
	 * @param canvasManager
	 */
	public void setCanvasManager ( CanvasManager canvasManager )
	{
		this.canvasManager = canvasManager;
	}

	/**
	 * Adds a relationship to the <code>ArrayList</code> of relationships for bookkeeping and modifying the
	 * relationships. This should only be called by a <code>Relationship</code>.
	 *
	 * @param relationship The relationship to be added to this <code>ClassBox</code>.
	 */
	public void addRelationship ( Relationship relationship )
	{
		relationships.add ( relationship );
	}

	/**
	 * Adds this class box to the canvas. Then adds the relationships of this <code>ClassBox</code> to the
	 * canvas by invoking the relationships' <code>addSelf ()</code> methods.
	 */
	public void addSelf ( )
	{
		if ( canvasManager.getCanvas ( ) != null )
		{
			canvasManager.getCanvas ( ).getChildren ( ).add ( this );
		}
	}

	/**
	 * This removes the class box and its relationships from the canvas.
	 * <p/>
	 * Removes all of the relationships of this <code>ClassBox</code> from this <code>ClassBox</code> by invoking on
	 * the
	 * relationships to remove themselves. Then, it removes itself from the canvas.
	 */
	public void removeSelf ( )
	{
		if ( canvasManager.getCanvas ( ) != null )
		{
			removeAllRelationships ( );

			canvasManager.getCanvas ( ).getChildren ( ).remove ( this );
		}
	}

	/**
	 * Removes the given relationship from this box's record. This is book keeping, invoked by the relationship.
	 * <p/>
	 * This method does so by searching for the given relationship and then removing it from the
	 * <code>ArrayList</code>.
	 *
	 * @param relationship
	 */
	public void removeRelationship ( Relationship relationship )
	{
		for ( Relationship rel : relationships )
		{
			if ( rel.equals ( relationship ) )
			{
				relationships.remove ( relationship );
				break;
			}
		}
	}

	/**
	 * Forces all relationships of this class box to remove themselves from the canvas and removes them from this
	 * class box's bookkeeping.
	 * <p/>
	 * It does this by calling on each relationship to remove itself until the <code>ArrayList</code> of relationships
	 * is empty.
	 */
	public void removeAllRelationships ( )
	{
		// TODO Due to how I have a relationship remove itself, this is VERY inefficient.
		while ( !relationships.isEmpty ( ) )
		{
			relationships.get ( 0 ).removeSelf ( );
		}
	}

	/**
	 * Updates all relationships tied to this class box.
	 * <p/>
	 * It does so by iterating through the
	 */
	public void updateRelationships ( )
	{
		for ( Relationship rel : relationships )
		{
			rel.update ( );
		}
	}

	/**
	 * Returns a copy of the relationships, as to disallow some tampering with the data structure.
	 * <p/>
	 * This is necessary for the undo feature, in that it allows the saving of relationships to give to a destroyed
	 * <code>ClassBox</code> after all of its relationships have removed themselves.
	 *
	 * @return A copy of the relationships.
	 */
	public ArrayList< Relationship > copyRelationships ( )
	{
		return new ArrayList< Relationship > ( relationships );
	}

	public CanvasManager getCanvasManager ( )
	{
		return canvasManager;
	}

	/**
	 * Returns this instance:  for use in listeners or handlers.
	 *
	 * @return This instance.
	 */
	private ClassBox getThis ( )
	{
		return this;
	}
}
