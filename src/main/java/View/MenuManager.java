package View;

import javafx.scene.Parent;

/**
 * This manages the tools and buttons of the menu. This is in charge of the initialization and ownership of the
 * tools and buttons.
 *
 * @author United Federation of Funk
 */
public class MenuManager
{
	private Parent root;
	private CanvasManager canvasManager; // TODO Consider eliminating this by changing the constructor. - EMD
	ToolBox toolBox;
	Buttons buttons;

	/**
	 * Constructs the <code>MenuManager</code> so that the menu buttons are assigned to affect the given
	 * <code>CanvasManager</code>.
	 *
	 * @param cm The <code>CanvasManger</code> that the menu buttons will affect.
	 */
	MenuManager ( CanvasManager cm )
	{
		canvasManager = cm;
	}

	/**
	 * Sets the .fxml GUI information source.
	 *
	 * @param parent <code>Parent</code> for the .fxml file.
	 */
	public void setParent ( Parent parent )
	{
		root = parent;
	}

	/**
	 * Initializes the menu buttons.
	 * <p>
	 * This includes the toggle buttons.
	 *
	 * @param root The .fxml GUI formatting information.
	 * @param canvasManager The <code>CanvasManager</code> that the buttons will interact with.
	 */
	public void initButtons ( Parent root, CanvasManager canvasManager )
	{
		toolBox = new ToolBox ( root, canvasManager );
		buttons = new Buttons ( root, canvasManager );
	}
}