package View;

import BoxObjects.ClassBox;
import CommandObjects.*;
import Relationships.*;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * Manages all canvases of the GUI.
 *
 * @author United Federation of Funk
 */
public class CanvasManager
{
	public static final boolean DEBUG = false;

	Pane canvas; // The canvas that is manages.
	public CommandAcceptor commandAcceptor = new CommandAcceptor ( ); // A command acceptor to connect to the command object structure.

	ToolType currentTool = ToolType.NONE_SELECTED; // Set to nothing as default. - EMD
	ClassBox selected = null;

	// DEBATE I suggest the drag information be put into the ClassBox - Eric
	/** Used to hold the initial position of the drag for the move command object. */
	double initialX, initialY;
	/** Used to hold the difference of the the class box from mouse drag location for dragging. */
	double offsetX, offsetY;
	ClassBox firstSelectedClassBoxForRelationship = null; // Used to save class box until other is selected.
	private FileManager fileManager;

	/**
	 * Mimics the default constructor.
	 */
	public CanvasManager ( )
	{
	}

	/**
	 * Constructs a <code>CanvasManager</code> with the canvas given to manage.
	 * <p/>
	 * This is the alternative to using the default constructor and setting the canvas afterwards.
	 *
	 * @param c The canvas to manage.
	 */
	CanvasManager ( Pane c )
	{
		canvas = c;
	}

	/**
	 * Assigns the <code>Canvas</code> to be managed.
	 *
	 * @param c The canvas to assign.
	 */
	public void setCanvas ( Pane c )
	{
		canvas = c;
	}

	/**
	 * Enacts the starting state of the main canvas.
	 *
	 * @deprecated Currently, this does nothing, and is mostly used for experimentation.
	 */
	public void initCanvas ( )
	{

	}

	/**
	 * Assigns tool-dependent mouse actions to the canvas.
	 */
	public void pollCanvas ( )
	{

		canvas.setOnMouseEntered ( new EventHandler< MouseEvent > ( )
		{
			@Override
			public void handle ( MouseEvent event )
			{
				canvas.requestFocus ( );
			}
		} );

		//switch on tooltype, place event handlers according to corresponding tooltype
		canvas.setOnMousePressed ( new EventHandler< MouseEvent > ( )
		{
			public void handle ( MouseEvent mouseEvent )
			{
				switch ( currentTool )
				{
					case NONE_SELECTED:
						break;

					case CREATE_CLASS_BOX:
						double clickLocationX = mouseEvent.getX ( );
						double clickLocationY = mouseEvent.getY ( );

						if ( DEBUG ) print ( "X: " + clickLocationX + " Y: " + clickLocationY + "\n" );

						addUMLObject ( clickLocationX, clickLocationY );
						mouseEvent.consume ( );
						break;
				}
			}
		} );
	}

	/**
	 * Enables the text editing in nodes that have text. This iterates through all children of the canvas.
	 */
	public void enableText ( )
	{
		for ( Node cb : canvas.getChildren ( ) )
		{
			if ( cb instanceof ClassBox )
			{
				( (ClassBox) cb ).enableTextArea ( );
			} else if ( cb instanceof Relationship )
			{
				( (Relationship) cb ).enableTextFields ( );
			}
		}
	}

	/**
	 * Disables the text editing in nodes that have text. This iterates through all children of the canvas.
	 */
	public void disableText ( )
	{
		for ( Node cb : canvas.getChildren ( ) )
		{
			if ( cb instanceof ClassBox )
			{
				( (ClassBox) cb ).disableTextArea ( );
			} else if ( cb instanceof Relationship )
			{
				( (Relationship) cb ).disableTextFields ( );
			}
		}
	}

	/**
	 * Sets the tool that decides the actions of canvas elements.
	 *
	 * @param newTool The tool to assign.
	 */
	public void setCurrentTool ( ToolType newTool )
	{
		print ( "\nCalled setCurrentTool (...)" );
		if ( currentTool != newTool )
			print ( " and the tool was changed" );
		print ( ".\n" );
		print ( "\tTool was " + currentTool.name ( ) + " and is now " + newTool.name ( ) + "\n" );

		currentTool = newTool;

		if ( currentTool == ToolType.EDIT_TEXT )
		{
			enableText ( );
		} else
		{
			disableText ( );
		}
	}

	/**
	 * Returns an <code>ToolType</code> enum value indicating the currently selected tool or lack thereof.
	 *
	 * @return An enum value indicating the currently selected tool or lack thereof.
	 */
	public ToolType getCurrentTool ( )
	{
		return currentTool;
	}

	/**
	 * Creates a rectangle and adds it to the canvas.
	 * <p>
	 * The rectangles are given <code>EventHandler</code> objects for tool-dependent mouse actions.
	 *
	 * @param xPos The X coordinate of the top-left corner of the rectangle to be added.
	 * @param yPos The Y coordinate of the top-left corner of the rectangle to be added.
	 */
	public void addUMLObject ( double xPos, double yPos )
	{
		double xTranslation = Math.max ( 0, xPos - ClassBox.DEFAULT_WIDTH / 2 );
		double yTranslation = Math.max ( 0, yPos - ClassBox.DEFAULT_HEIGHT / 2 );
		final ClassBox classBox = new ClassBox ( xTranslation, yTranslation );
		classBox.setCanvasManager ( this );

		classBox.setOnMousePressed ( new EventHandler< MouseEvent > ( )
		{
			@Override
			public void handle ( MouseEvent event )
			{
				switch ( currentTool )
				{
					case DELETE:
						DestroyObject destroyObject = new DestroyObject ( classBox );
						commandAcceptor.acceptCommand ( destroyObject );
						break;

					// Gathers information for the initial click.
					case DRAG:
						classBox.toFront ( );
						classBox.setCursor ( Cursor.CLOSED_HAND );

						initialX = classBox.getTranslateX ( );
						initialY = classBox.getTranslateY ( );
						offsetX = classBox.getTranslateX ( ) - event.getSceneX ( );
						offsetY = classBox.getTranslateY ( ) - event.getSceneY ( );
						break;

					case EDIT_TEXT:
						selected = classBox;
						classBox.toFront ( );
						break;

					case CREATE_AGGREGATION:
						if ( firstSelectedClassBoxForRelationship == null )
							firstSelectedClassBoxForRelationship = classBox;
						else
						{
							Aggregation a = new Aggregation ( firstSelectedClassBoxForRelationship, classBox );
							establishRelationship ( a );
						}
						break;

					case CREATE_UNDIRECTED_ASSOCIATION:
						if ( firstSelectedClassBoxForRelationship == null )
							firstSelectedClassBoxForRelationship = classBox;
						else
						{
							UndirectedAssociation c = new UndirectedAssociation ( firstSelectedClassBoxForRelationship, classBox );
							establishRelationship ( c );
						}
						break;

					case CREATE_DIRECTED_ASSOCIATION:
						if ( firstSelectedClassBoxForRelationship == null )
							firstSelectedClassBoxForRelationship = classBox;
						else
						{
							DirectedAssociation c = new DirectedAssociation ( firstSelectedClassBoxForRelationship, classBox );
							establishRelationship ( c );
						}
						break;

					case CREATE_REALIZATION:
						if ( firstSelectedClassBoxForRelationship == null )
							firstSelectedClassBoxForRelationship = classBox;
						else
						{
							Realization r = new Realization ( firstSelectedClassBoxForRelationship, classBox );
							establishRelationship ( r );
						}
						break;

					case CREATE_COMPOSITION:
						if ( firstSelectedClassBoxForRelationship == null )
							firstSelectedClassBoxForRelationship = classBox;
						else
						{
							Composition c = new Composition ( firstSelectedClassBoxForRelationship, classBox );
							establishRelationship ( c );
						}
						break;

					case CREATE_DEPENDENCY:
						if ( firstSelectedClassBoxForRelationship == null )
							firstSelectedClassBoxForRelationship = classBox;
						else
						{
							Dependency c = new Dependency ( firstSelectedClassBoxForRelationship, classBox );
							establishRelationship ( c );
						}
						break;

					case CREATE_GENERALIZATION:
						if ( firstSelectedClassBoxForRelationship == null )
							firstSelectedClassBoxForRelationship = classBox;
						else
						{
							Generalization c = new Generalization ( firstSelectedClassBoxForRelationship, classBox );
							establishRelationship ( c );
						}
						break;
				}
			}

			/**
			 * This is used to draw a relationship on the canvas. It does so through the command object structure.
			 * Additionally, it adds a mouse listener that enables deletion, provided that the deletion tool is
			 * selected.
			 * <p/>
			 * This is required for the algorithm that governs which class boxes become related.
			 * @param relationship
			 */
			private void establishRelationship ( final Relationship relationship )
			{
				relationship.setOnMousePressed ( new EventHandler< MouseEvent > ( )
				{
					@Override
					public void handle ( MouseEvent event )
					{
						if ( currentTool == ToolType.DELETE )
						{
							DestroyRelationship d = new DestroyRelationship ( relationship );
							commandAcceptor.acceptCommand ( d );
						}
					}
				} );
				relationship.setCanvasManager ( getThis ( ) );

				relationship.updateMouseEventHandlers ( );

				CreateRelationship r = new CreateRelationship ( relationship );
				commandAcceptor.acceptCommand ( r );
				firstSelectedClassBoxForRelationship = null;
			}
		} );

		classBox.setOnMouseReleased ( new EventHandler< MouseEvent > ( )
		{
			@Override
			public void handle ( MouseEvent event )
			{
				switch ( currentTool )
				{
					case DRAG:
						classBox.setCursor ( Cursor.OPEN_HAND );
						MoveObject m = new MoveObject ( classBox, initialX, initialY, classBox.getTranslateX ( ), classBox.getTranslateY ( ) );
						commandAcceptor.acceptCommand ( m );
				}
			}
		} );

		// Relocates the rectangle by translation from its original position.
		classBox.setOnMouseDragged ( new EventHandler< MouseEvent > ( )
		{
			@Override
			public void handle ( MouseEvent event )
			{
				if ( currentTool == ToolType.DRAG )
				{

					classBox.toFront ( );

					double newX = event.getSceneX ( ) + offsetX;
					double newY = event.getSceneY ( ) + offsetY;

					classBox.setTranslateX ( newX > 0 ? newX : 0 );
					classBox.setTranslateY ( newY > 0 ? newY : 0 );

					classBox.updateRelationships ( );

					event.consume ( );
				}
			}
		} );

		// NOTE This has to be at the end, so that the COMPLETED class box is in the command object.
		CreateObject createObject = new CreateObject ( classBox );
		commandAcceptor.acceptCommand ( createObject );
	}

	/**
	 * Used for debugging only, dependent on the <code>DEBUG</code> global variable.
	 *
	 * @param message The message to print.
	 * TODO We can probably look into tests wherever this method is used.
	 */
	public static void print ( String message )
	{
		if ( DEBUG )
			System.out.print ( message );
	}

	/**
	 * This is called for the case of different relationship tools using clicks on class boxes. Ordinarily, this would
	 * cause a relationship to be drawn of the second relationship tool used. The <code>ToolBox</code> informs the
	 * <code>CanvasManager</code> when a tool is switched so that any initial click on a class box with a relationship
	 * tool selected will no longer be considered for creating a relationship.
	 * <p/>
	 * It does this by resetting an instance variable to null, which is essential for the algorithm.
	 */
	public void resetRelationshipTools ( )
	{
		firstSelectedClassBoxForRelationship = null;
	}

	/**
	 * Returns this instance - used for EventHandlers.
	 *
	 * @return This instance;
	 */
	private CanvasManager getThis ( )
	{
		return this;
	}

	/**
	 * Returns the canvas of this manager - smaller components tend to refer to it.
	 *
	 * @return The canvas of this manager.
	 */
	public Pane getCanvas ( )
	{
		return canvas;
	}

	/**
	 * Sets the <code>FileManager</code> that this object will use.
	 *
	 * @param fileManager The <code>FileManager</code> that this object will use.
	 */
	public void setFileManager ( FileManager fileManager )
	{
		this.fileManager = fileManager;
	}

	/**
	 * Retrieves the <code>FileManager</code> that this object uses, for use by other objects requesting file
	 * operations.
	 *
	 * @return The <code>FileManager</code> that this object uses.
	 */
	public FileManager getFileManager ( )
	{
		return fileManager;
	}
}
