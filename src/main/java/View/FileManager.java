package View;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.*;

/**
 * The file manager is responsible for starting the saving and opening processes.
 */
public class FileManager
{
	/** The file that is currently in use. Used for saving without the Save As. */
	private static File file;
	private FileOutputStream fos;
	private ObjectOutputStream oos;
	private FileInputStream fis;
	private ObjectInputStream ois;
	private Stage stage;

	/**
	 * Constructs a <code>FileManager</code> with no arguments for the purpose of object saving and loading only.
	 */
	public FileManager ( )
	{
	}

	/**
	 * Constructs a <code>FileManager</code> with a stage for dialog windows.
	 *
	 * @param stage The stage to which the dialog windows will belong to.
	 */
	public FileManager ( Stage stage )
	{
		this.stage = stage;
	}

	/**
	 * This is called to open a file and build a canvas using the information in the file. If a user does not open a
	 * file, then nothing happens. Otherwise, this method assigns a loaded canvas to the <code>CanvasManager</code>
	 * argument.
	 *
	 * @param canvasManager The canvas manager to which the loaded canvas will be assigned.
	 */
	public void open ( CanvasManager canvasManager )
	{
		// Get the file ready.
		FileChooser fileChooser = new FileChooser ( );
		fileChooser.setTitle ( "Welcome To the Funky File Opener!" );
		File tempFile = fileChooser.showOpenDialog ( stage );
		if ( tempFile != null )
		{
			if ( CanvasManager.DEBUG )
			{
				System.out.println ( tempFile.getPath ( ) ); // DEBUG
			}
			file = tempFile;
			setInputStreams ( );

			Pane canvas;
			try
			{
				canvas = (Pane) ois.readObject ( );
				canvasManager.setCanvas ( canvas );
			} catch ( IOException e )
			{
				e.printStackTrace ( );
			} catch ( ClassNotFoundException e )
			{
				e.printStackTrace ( );
			}
		}
	}

	/**
	 * This is called to save a canvas to a file that is yet to be specified.
	 *
	 * @param canvasManager The canvas manager that holds the canvas to be saved.
	 */
	public void saveAs ( CanvasManager canvasManager )
	{
		FileChooser fileChooser = new FileChooser ( );
		fileChooser.setTitle ( "Welcome To the Funky File Saver!" );
		File tempFile = fileChooser.showSaveDialog ( stage );

		if ( tempFile != null )
		{
			file = tempFile;
			setOutputStreams ( );
			try
			{
				oos.writeObject ( canvasManager );
			} catch ( IOException e )
			{
				e.printStackTrace ( );
			}
		}
	}

	/**
	 * Exports the canvas area as a .png file for later viewing
	 *
	 * @param canvasManager: holds the canvas to be exported
	 */
	public void exportAsPNG ( CanvasManager canvasManager )
	{
		WritableImage image = canvasManager.getCanvas ( ).snapshot ( new SnapshotParameters ( ), null );
		FileChooser fileHandler = new FileChooser ( );
		fileHandler.setTitle ( "Export Your Diagram" );
		fileHandler.getExtensionFilters ( ).add ( new FileChooser.ExtensionFilter ( "PNG Image", "*.png" ) );

		File exportDoc = fileHandler.showSaveDialog ( canvasManager.getCanvas ( ).getScene ( ).getWindow ( ) );

		if ( exportDoc != null )
		{

			if ( !exportDoc.getPath ( ).endsWith ( ".png" ) )
			{
				exportDoc = new File ( exportDoc.getPath ( ) + ".png" );
			}
			try
			{
				ImageIO.write ( SwingFXUtils.fromFXImage ( image, null ), "png", exportDoc );
			} catch ( IOException ioe )
			{

			}
		}
	}

	/**
	 * This is called to save a canvas to the current file. If there is no current file, then this defaults to the
	 * <code>saveAs</code> method of this class.
	 *
	 * @param canvasManager The canvas manager that holds the canvas to be saved.
	 */
	public void save ( CanvasManager canvasManager )
	{
		if ( file != null )
		{
			try
			{
				oos.writeObject ( canvasManager );
			} catch ( IOException e )
			{
				e.printStackTrace ( );
			}
		} else
		{
			saveAs ( canvasManager );
		}
	}

	/**
	 * This sets the input streams to the current file. If there is no current file (<code>file == null</code>), then
	 * this method does nothing.
	 */
	private void setInputStreams ( )
	{
		if ( file != null )
		{
			try
			{
				fis = new FileInputStream ( file );
			} catch ( FileNotFoundException e )
			{
				e.printStackTrace ( );
			}
			try
			{
				ois = new ObjectInputStream ( fis );
			} catch ( IOException e )
			{
				e.printStackTrace ( );
			}
		}
	}

	/**
	 * This sets the output streams to the current file. If there is no current file (<code>file == null</code>), then
	 * this method does nothing.
	 */
	private void setOutputStreams ( )
	{
		if ( file != null )
		{
			try
			{
				fos = new FileOutputStream ( file );
			} catch ( FileNotFoundException e )
			{
				e.printStackTrace ( );
			}
			try
			{
				oos = new ObjectOutputStream ( fos );
			} catch ( IOException e )
			{
				e.printStackTrace ( );
			}
		}
	}
}
