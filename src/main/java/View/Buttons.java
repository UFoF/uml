package View;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;

/**
 * This class contains and manages the buttons that do not represent tools to modify the diagram. That is, this
 * manages the undo and redo buttons.
 */
public class Buttons
{
	Button save;
	Button open;
	Button undo;
	Button redo;
	public static final boolean DEBUG = CanvasManager.DEBUG;

	/**
	 * Constructs this object to pull button information from fxml source and work with the given
	 * <code>CanvasManager</code>.
	 *
	 * @param root This is where all of the fxml information will be extracted from.
	 * @param canvasManager This
	 */
	public Buttons ( Parent root, final CanvasManager canvasManager )
	{
		save = (Button) root.lookup ( "#SaveBox" );
		open = (Button) root.lookup ( "#OpenBox" );
		undo = (Button) root.lookup ( "#undoButton" );
		redo = (Button) root.lookup ( "#redoButton" );

		undo.setOnAction ( new EventHandler< ActionEvent > ( )
		{
			@Override
			public void handle ( ActionEvent event )
			{
				if ( DEBUG )
				{
					System.out.println ( "Undo was selected." );
				}
				canvasManager.commandAcceptor.undoCommand ( );
			}
		} );

		redo.setOnAction ( new EventHandler< ActionEvent > ( )
		{
			@Override
			public void handle ( ActionEvent event )
			{
				if ( DEBUG )
				{
					System.out.println ( "Redo was selected." );
				}
				canvasManager.commandAcceptor.redoCommand ( );
			}
		} );

		save.setOnAction ( new EventHandler< ActionEvent > ( )
		{
			@Override
			public void handle ( ActionEvent event )
			{
				if ( DEBUG )
				{
					System.out.println ( "Save was selected." );
				}
				canvasManager.getFileManager ( ).exportAsPNG ( canvasManager );
			}
		} );
	}
}
