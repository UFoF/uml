package View;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;

/**
 * This holds the toggle buttons of the GUI and assures their connection to the <code>CanvasManger</code> that's
 * passed to the constructor.
 *
 * @author United Federation of Funk
 */
public class ToolBox
{
	CanvasManager canvasManager;
	ToggleGroup toggleGroup = new ToggleGroup ( );
	ToggleButton editText,
			dragTool,
	//			packageTool,
	//			objectBox,
	//			textBox,
	dependencyTool,
			aggregationTool,
			realizationTool,
			compositionTool,
			classBox,
			jointTool,
			undirectedAssociationTool,
			generalizationTool,
			directedAssociationTool,
			deleteTool,
	//			saveTool,
	panTool;
	//            zoomInTool,
	//            zoomOutTool;

	/** Event handler to communicate with the canvas manager when a toggle is pressed. */
	// TODO Make this more efficient by having many handlers and no ifs.
	EventHandler< ActionEvent > toggleHandler =
			new EventHandler< ActionEvent > ( )
			{
				public void handle ( ActionEvent event )
				{
					ToggleButton currentToggle = (ToggleButton) toggleGroup.getSelectedToggle ( );
					canvasManager.resetRelationshipTools ( );
					if ( currentToggle == null )
					{
						canvasManager.setCurrentTool ( ToolType.NONE_SELECTED );
					} else if ( currentToggle == editText )
					{
						canvasManager.setCurrentTool ( ToolType.EDIT_TEXT );
					} else if ( currentToggle == dragTool )
					{
						canvasManager.setCurrentTool ( ToolType.DRAG );
						//					} else if ( currentToggle == packageTool )
						//					{
						//						canvasManager.setCurrentTool ( ToolType.CREATE_PACKAGE );
						//					} else if ( currentToggle == objectBox )
						//					{
						//						// TODO DEBUG DELETE
						//						canvasManager.getFileManager ( ).open ( canvasManager );
						//						canvasManager.setCurrentTool ( ToolType.CREATE_OBJECT_BOX );
						//					} else if ( currentToggle == textBox )
						//					{
						//						canvasManager.setCurrentTool ( ToolType.CREATE_TEXT_BOX );
					} else if ( currentToggle == dependencyTool )
					{
						canvasManager.setCurrentTool ( ToolType.CREATE_DEPENDENCY );
					} else if ( currentToggle == aggregationTool )
					{
						canvasManager.setCurrentTool ( ToolType.CREATE_AGGREGATION );
					} else if ( currentToggle == realizationTool )
					{
						canvasManager.setCurrentTool ( ToolType.CREATE_REALIZATION );
					} else if ( currentToggle == compositionTool )
					{
						canvasManager.setCurrentTool ( ToolType.CREATE_COMPOSITION );
					} else if ( currentToggle == classBox )
					{
						canvasManager.setCurrentTool ( ToolType.CREATE_CLASS_BOX );
					} else if ( currentToggle == jointTool )
					{
						// TODO Change this to whatever - I changed it for testing.
						//							canvasManager.setCurrentTool ( ToolType.CREATE_ITEM_BOX );
						canvasManager.setCurrentTool ( ToolType.CREATE_JOINT );
					} else if ( currentToggle == undirectedAssociationTool )
					{
						canvasManager.setCurrentTool ( ToolType.CREATE_UNDIRECTED_ASSOCIATION );
					} else if ( currentToggle == generalizationTool )
					{
						canvasManager.setCurrentTool ( ToolType.CREATE_GENERALIZATION );
					} else if ( currentToggle == directedAssociationTool )
					{
						canvasManager.setCurrentTool ( ToolType.CREATE_DIRECTED_ASSOCIATION );
					} else if ( currentToggle == deleteTool )
					{
						canvasManager.setCurrentTool ( ToolType.DELETE );
						//					} else if ( currentToggle == saveTool )
						//					{
						//
						//						canvasManager.getFileManager ( ).saveAs ( canvasManager );
						//						canvasManager.setCurrentTool ( ToolType.SAVE_AS );
						//					}else if ( currentToggle == zoomInTool )
						//                    {
						//                        canvasManager.setCurrentTool ( ToolType.ZOOM_IN );
					} else if ( currentToggle == panTool )
					{
						canvasManager.setCurrentTool ( ToolType.PAN );
						//                    } else if ( currentToggle == zoomOutTool )
						//                    {
						//                        canvasManager.setCurrentTool ( ToolType.ZOOM_OUT );
					} else
					{
						// canvasManager.print ( "Tool is not set!!!\n" );
					}
				} // end method handle
			};

	/**
	 * Constructs a <code>ToolBox</code>.
	 * <p>
	 * Most significantly, it initializes each <code>ToggleButton</code> with an <code>EventHandler</code>
	 * and <code>ToggleGroup</code>.
	 *
	 * @param canvasManagerIn <code>CanvasManager</code> to be affected by the tools indicated by the toggle buttons.
	 */
	public ToolBox ( Parent root, final CanvasManager canvasManagerIn )
	{
		//		Parent root = null;
		//		try
		//		{
		//			root = FXMLLoader.load ( getClass ( ).getResource ( Main.LAYOUT_FILE_NAME ) );
		//		} catch ( IOException e )
		//		{
		//			e.printStackTrace ( );
		//		}
		this.canvasManager = canvasManagerIn;

		editText = (ToggleButton) root.lookup ( "#textEditTool" );
		editText.setToggleGroup ( toggleGroup );
		editText.setOnAction ( toggleHandler );

		dragTool = (ToggleButton) root.lookup ( "#dragTool" );
		dragTool.setToggleGroup ( toggleGroup );
		dragTool.setOnAction ( toggleHandler );

		dependencyTool = (ToggleButton) root.lookup ( "#dependencyTool" );
		dependencyTool.setToggleGroup ( toggleGroup );
		dependencyTool.setOnAction ( toggleHandler );

		aggregationTool = (ToggleButton) root.lookup ( "#aggregationTool" );
		aggregationTool.setToggleGroup ( toggleGroup );
		aggregationTool.setOnAction ( toggleHandler );

		realizationTool = (ToggleButton) root.lookup ( "#realizationTool" );
		realizationTool.setToggleGroup ( toggleGroup );
		realizationTool.setOnAction ( toggleHandler );

		compositionTool = (ToggleButton) root.lookup ( "#compositionTool" );
		compositionTool.setToggleGroup ( toggleGroup );
		compositionTool.setOnAction ( toggleHandler );

		classBox = (ToggleButton) root.lookup ( "#classBox" );
		classBox.setToggleGroup ( toggleGroup );
		classBox.setOnAction ( toggleHandler );

		jointTool = (ToggleButton) root.lookup ( "#itemBox" );
		jointTool.setToggleGroup ( toggleGroup );
		jointTool.setOnAction ( toggleHandler );

		undirectedAssociationTool = (ToggleButton) root.lookup ( "#undirectedAssociationTool" );
		undirectedAssociationTool.setToggleGroup ( toggleGroup );
		undirectedAssociationTool.setOnAction ( toggleHandler );

		generalizationTool = (ToggleButton) root.lookup ( "#generalizationTool" );
		generalizationTool.setToggleGroup ( toggleGroup );
		generalizationTool.setOnAction ( toggleHandler );

		directedAssociationTool = (ToggleButton) root.lookup ( "#directedAssociationTool" );
		directedAssociationTool.setToggleGroup ( toggleGroup );
		directedAssociationTool.setOnAction ( toggleHandler );

		deleteTool = (ToggleButton) root.lookup ( "#deleteTool" );
		deleteTool.setToggleGroup ( toggleGroup );
		deleteTool.setOnAction ( toggleHandler );

		// TODO TEST. Can we make sure that all toggles are non-null and have actionListeners assigned?
	}
}
