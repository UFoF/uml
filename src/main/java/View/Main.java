package View;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * This class starts the application.
 */
public class Main extends Application
{
	//Global variables
	CanvasManager cm = new CanvasManager ( );
	MenuManager mm = new MenuManager ( cm );
	public final static String LAYOUT_FILE_NAME = "/layout.fxml";

	@FXML
	public Pane canvas;

	/**
	 * Starts the program.
	 *
	 * @param args
	 */
	public static void main ( String[] args )
	{
		launch ( args );
	}

	/**
	 * Creates the primary window and assigns its properties.
	 *
	 * @param primaryStage The primary stage.
	 *
	 * @throws java.io.IOException Thrown in the case of an error loading the .fxml GUI layout information.
	 */
	@Override
	public void start ( Stage primaryStage ) throws IOException
	{

		Parent root = FXMLLoader.load ( getClass ( ).getResource ( LAYOUT_FILE_NAME ) );
		double screenWidth = Screen.getPrimary ( ).getBounds ( ).getWidth ( );
		double screenHeight = Screen.getPrimary ( ).getBounds ( ).getHeight ( );
		primaryStage.setScene ( new Scene ( root, screenWidth * 3 / 4, screenHeight * 3 / 4 ) );
		primaryStage.setTitle ( "Welcome to Funkytown!" );
		primaryStage.show ( );

		FileManager fileManager = new FileManager ( primaryStage );

		canvas = (Pane) root.lookup ( "#canvas" );
		cm.setCanvas ( canvas );
		cm.setFileManager ( fileManager );
		cm.initCanvas ( ); // Used for testing purposes, currently.
		cm.pollCanvas ( );

		mm.setParent ( root );
		mm.initButtons ( root, cm );
	}
}