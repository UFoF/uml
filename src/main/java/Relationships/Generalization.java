package Relationships;

import BoxObjects.ClassBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;

/**
 * This relationship is characterized by a solid line and a empty triangle as the arrowhead.
 */
public class Generalization extends Relationship
{
	Polygon triangle; // The triangle that serves as the arrowhead for the relationship.

	/**
	 * Constructs a generalization relationship.
	 * <p/>
	 * This relationship is characterized by a solid jointedLine and a empty triangle as the arrowhead.
	 *
	 * @param child The child of the relationship (tail end).
	 * @param parent The parent of the relationship (head end).
	 */
	public Generalization ( ClassBox child, ClassBox parent )
	{
		super ( child, parent );
		triangle = new Polygon ( -TRIANGLE_HEIGHT, -HALF_TRIANGLE_BASE, 0, 0, -TRIANGLE_HEIGHT, HALF_TRIANGLE_BASE );
		triangle.setStroke ( LINE_COLOR );
		triangle.setFill ( Color.WHITE );
		triangle.setStrokeWidth ( STROKE_WIDTH );

		getChildren ( ).add ( triangle );
		update ( );
	}

	@Override
	public void formatLine ( Line line )
	{
		super.formatLine ( line );
		line.setStroke ( LINE_COLOR );
		line.setStrokeWidth ( STROKE_WIDTH );
	}

	/**
	 * Sets the triangle to also cause removal of this relationship.
	 */
	@Override
	public void updateMouseEventHandlers ( )
	{
		triangle.setOnMousePressed ( getOnMousePressed ( ) );
		jointedLine.setOnMousePressed ( getOnMousePressed ( ) );
	}
}
