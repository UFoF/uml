package Relationships;

import BoxObjects.ClassBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;

/**
 * This relationship is characterized by a solid line and a filled-in diamond as the arrowhead.
 */
public class Composition extends Relationship
{

	Rectangle filledDiamond; // The diamond that appears as the arrowhead of the relationship.

	/**
	 * Constructs a composition relationship.
	 * <p/>
	 * This relationship is characterized by a solid line and a filled-in diamond as the arrowhead.
	 *
	 * @param child The child of the relationship (tail end).
	 * @param parent The parent of the relationship (head end).
	 */
	public Composition ( ClassBox child, ClassBox parent )
	{
		super ( child, parent );
		filledDiamond = new Rectangle ( 15, 15, Color.WHITE );
		filledDiamond.setStroke ( LINE_COLOR );
		filledDiamond.setFill ( LINE_COLOR );
		filledDiamond.setStrokeWidth ( STROKE_WIDTH );
		filledDiamond.getTransforms ( ).add ( new Rotate ( 135, 0, 0 ) );

		getChildren ( ).add ( filledDiamond );
		update ( );
	}

	@Override
	public void formatLine ( Line line )
	{
		super.formatLine ( line );
		line.setStroke ( LINE_COLOR );
		line.setStrokeWidth ( STROKE_WIDTH );
	}

	/**
	 * Causes a mouse click on the diamond and recursive jointedLine to cause the removal of this relationship.
	 */
	@Override
	public void updateMouseEventHandlers ( )
	{
		filledDiamond.setOnMousePressed ( getOnMousePressed ( ) );
		jointedLine.setOnMousePressed ( getOnMousePressed ( ) );
	}
}
