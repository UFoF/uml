package Relationships;

import BoxObjects.ClassBox;
import RelationshipAuxilliary.IntersectionInfo;
import RelationshipAuxilliary.JointedLine;
import RelationshipAuxilliary.MiddleTextInfo;
import RelationshipAuxilliary.RelationshipTextField;
import View.CanvasManager;
import View.ToolType;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.transform.Rotate;

/**
 * This is the abstract class that is implemented to create a fully defined object that represents a relationship
 * in a UML Class Diagram. It also uses constants to facilitate the keeping of a standard for the visual aspects
 * such as jointedLine width and color.
 */
public abstract class Relationship extends Group
{
	// The following constants are here to format the lines and arrowheads.
	/** Indicates the width of the lines for lines and borders of polygons. */
	final static double STROKE_WIDTH = 3.0;
	/** Sets the length of dashes of a dashed line. */
	final static double DASH_LENGTH = 10.0;
	/** Used as the basis for all arrowhead sizes. Sets isosceles triangle and arrow dimensions. */
	final static double HALF_TRIANGLE_HEIGHT = 8.6602540;
	final static double TRIANGLE_HEIGHT = 2 * HALF_TRIANGLE_HEIGHT;
	final static double HALF_TRIANGLE_BASE = HALF_TRIANGLE_HEIGHT * 2 / Math.sqrt ( 3 );
	/** The standard color for lines and shapes of a relationship. */
	final static Color LINE_COLOR = Color.BLACK;
	/** The displacement of the cardinality text from a line / class box intersection along the line. */
	final static double CARDINALITY_DISPLACEMENT = 20;
	/** For text fields, the desired distance away from a use-specific reference point. */
	private static final double TEXT_DISTANCE = 30;
	public boolean isRecursive;

	protected ClassBox childReference, parentReference;
	/** Used to allow the Lines' handlers. */
	public CanvasManager canvasManager;
	/** The shape or shapes that define the arrowhead of the relationship. */
	JointedLine jointedLine;
	/** The cardinality text on the child side of the relationship. */
	protected RelationshipTextField childCardinality;
	/** The cardinality text on the parent side of the relationship. */
	protected RelationshipTextField parentCardinality;
	/** The label for the relationship, to be displayed on the middle line. */
	protected RelationshipTextField label;

	// DEBATE Could we have the text fields have be transparent normally, and opaque on mouse over so people see them?

	/**
	 * This constructs the default relationship, and exists only for loading from a file. That is,
	 * this should not be used by any other than the JVM.
	 * <p/>
	 * It is empty, and just sets the defaults.
	 */
	public Relationship ( )
	{
		// Do nothing.
	}

	/**
	 * This constructs a <code>Relationship</code> that is created to display between the <code>ClassBox</code> objects
	 * provided. For this constructor, the first class box is for the tail end of this relationship, and the second
	 * is for the head end.
	 *
	 * @param child The child of the relationship (tail end).
	 * @param parent The parent of the relationship (head end).
	 */
	public Relationship ( ClassBox child, ClassBox parent )
	{
		childReference = child;
		parentReference = parent;
		canvasManager = child.getCanvasManager ( );

		isRecursive = child == parent;

		if ( isRecursive )
		{
			constructRecursiveLine ( );
		} else
		{
			jointedLine = new JointedLine ( this );
		}

		initializeTextFields ( );
	}

	/**
	 * Meant to be called by the constructor, this method initializes all aspects of the text fields on the
	 * relationship.
	 */
	protected void initializeTextFields ( )
	{
		childCardinality = new RelationshipTextField ( canvasManager );
		parentCardinality = new RelationshipTextField ( canvasManager );
		//		label = new RelationshipTextField ( canvasManager, "Label" ); // DEBUG
		label = new RelationshipTextField ( canvasManager );
		childCardinality.setMouseTransparent ( true );
		parentCardinality.setMouseTransparent ( true );
		label.setMouseTransparent ( true );
	}

	/**
	 * This is used only in the case of a recursive relationship. This sets up a jointed jointedLine that leaves the
	 * class box
	 * out of the top, turns to the right, turns down, and turns left back into the class box.
	 */
	private void constructRecursiveLine ( )
	{
		double x = parentReference.getTranslateX ( );
		double y = parentReference.getTranslateY ( );
		double halfW = parentReference.getWidth ( ) / 2;
		double halfH = parentReference.getHeight ( ) / 2;

		final double distanceFromEdge = HALF_TRIANGLE_HEIGHT * 3;
        double X1 = x + halfW;
        double Y1 = y - distanceFromEdge;
        double X2 = x - 2 * distanceFromEdge;
        double Y2 = y - distanceFromEdge;
        double X3 = x - 2 * distanceFromEdge;
        double Y3 = y + halfH;
		jointedLine = new JointedLine ( this, X1, Y1, X2, Y2, X3, Y3 );
	}

	/**
	 * Returns the class box on the child end of the relationship.
	 *
	 * @return The class box on the child end of the relationship.
	 */
	public ClassBox getChildReference ( )
	{
		return childReference;
	}

	/**
	 * Returns the class box on the parent end of the relationship.
	 *
	 * @return The class box on the parent end of the relationship.
	 */
	public ClassBox getParentReference ( )
	{
		return parentReference;
	}

	/**
	 * Used for the relationship-specific addition of this relationship to records of its child and parent ends
	 * and the canvas.
	 */
	public void addSelf ( )
	{
		childReference.addRelationship ( this );
		parentReference.addRelationship ( this );
		if ( !canvasManager.getCanvas ( ).getChildren ( ).contains ( this ) )
		{
			canvasManager.getCanvas ( ).getChildren ( ).add ( this );

			toBack ( );

			canvasManager.getCanvas ( ).getChildren ( ).add ( childCardinality );
			canvasManager.getCanvas ( ).getChildren ( ).add ( parentCardinality );
			canvasManager.getCanvas ( ).getChildren ( ).add ( label );

			jointedLine.addSelf ( );
			jointedLine.toBack ( );
		}
	}

	/**
	 * Used for the relationship-specific removal of itself from the records of its child and parent ends
	 * and the canvas.
	 */
	public void removeSelf ( )
	{
		childReference.removeRelationship ( this );
		parentReference.removeRelationship ( this );
		canvasManager.getCanvas ( ).getChildren ( ).remove ( this );
		canvasManager.getCanvas ( ).getChildren ( ).remove ( jointedLine );

		canvasManager.getCanvas ( ).getChildren ( ).remove ( childCardinality );
		canvasManager.getCanvas ( ).getChildren ( ).remove ( parentCardinality );
		canvasManager.getCanvas ( ).getChildren ( ).remove ( label );

		jointedLine.removeSelf ( );
	}

	/**
	 * This gathers intersection information to set text locations and update the orientation of the arrow head
	 * depending on the orientation of the jointed line and the class boxes of this relationship.
	 */
	public void update ( )
	{
		/* PARENT */
		IntersectionInfo parentIntersect = findParentIntersectionInformation ( );
		// Set intersection coordinates.
		setTranslateX ( parentIntersect.xIntersect );
		setTranslateY ( parentIntersect.yIntersect );
		double angle = parentIntersect.angle;
		parentCardinality.setTranslateX ( parentIntersect.cardinalityX - parentCardinality.getPrefWidth ( ) / 2 );
		parentCardinality.setTranslateY ( parentIntersect.cardinalityY - parentCardinality.getPrefHeight ( ) / 2 );
		parentCardinality.toFront ( ); // TODO This is nasty.
		// Works out the new rotation for the arrowhead.
		double lineStartX = jointedLine.getEnd ( ).getStartX ( );
		double lineEndX = jointedLine.getEnd ( ).getEndX ( );
		if ( lineEndX < lineStartX )
		{
			angle += 180;
		}
		getTransforms ( ).setAll ( new Rotate ( angle ) );

		/* CHILD */
		IntersectionInfo childIntersect = findChildIntersectionInformation ( );
		childCardinality.setTranslateX ( childIntersect.cardinalityX - childCardinality.getPrefWidth ( ) / 2 );
		childCardinality.setTranslateY ( childIntersect.cardinalityY - childCardinality.getPrefHeight ( ) / 2 );
		childCardinality.toFront ( ); // TODO This is nasty.

		/* LABEL */
		MiddleTextInfo middleTextInfo = findMiddleTextInformation ( );
		label.setTranslateX ( middleTextInfo.centerX - label.getPrefWidth ( ) / 2 );
		label.setTranslateY ( middleTextInfo.centerY - label.getPrefHeight ( ) / 2 );
		label.getTransforms ( ).setAll ( new Rotate ( middleTextInfo.rotationAngleInDegrees,
		                                              label.getPrefWidth ( ) / 2, label.getPrefHeight ( ) / 2 ) );
		label.toFront ( ); // TODO This is nasty.
	}

	/**
	 * Uses <code>findClassBoxIntersectionInformation (...)</code> to find the intersection information
	 * for the child class box.
	 *
	 * @return The intersection info of an <code>IntersectInfo</code> object for the child class box.
	 */
	protected IntersectionInfo findChildIntersectionInformation ( )
	{
		return findClassBoxIntersectionInformation ( childReference, jointedLine.getStart ( ), false );
	}

	/**
	 * Uses <code>findClassBoxIntersectionInformation (...)</code> to find the intersection information
	 * for the parent class box.
	 *
	 * @return The intersection info of an <code>IntersectInfo</code> object for the parent class box.
	 */
	protected IntersectionInfo findParentIntersectionInformation ( )
	{
		return findClassBoxIntersectionInformation ( parentReference, jointedLine.getEnd ( ), true );
	}

	/**
	 * Calculates information concerning the layout relationship description text box on at the middle line. To return
	 * all relevant data at once, it uses a <code>MiddleTextInfo</code> object. The information returned is that of
	 * a <code>MiddleTextInfo</code> object.
	 *
	 * @return Information for the layout of the relationship label as a complete <code>MiddleTextInfo</code> object.
	 */
	protected MiddleTextInfo findMiddleTextInformation ( )
	{
		Line middle = jointedLine.getMiddle ( );
		double midpointX = ( middle.getEndX ( ) + middle.getStartX ( ) ) / 2;
		double midpointY = ( middle.getEndY ( ) + middle.getStartY ( ) ) / 2;
		double deltaY = middle.getEndY ( ) - middle.getStartY ( );
		double deltaX = middle.getEndX ( ) - middle.getStartX ( );
		double slope = deltaX == 0 ? Double.MAX_VALUE : deltaY / deltaX;
		double textAngleInDegrees = deltaX == 0 ? 90 : Math.toDegrees ( Math.atan ( slope ) );
		if ( deltaY == 0 ) // Trivial case.
		{
			return new MiddleTextInfo ( midpointX, midpointY - TEXT_DISTANCE, textAngleInDegrees );
		} else
		{
			/* The new point will be along the line that is perpendicular to the middle line at the midpoint.
			* This not-drawn perpendicular line has the opposite reciprocal slope of the middle line.
			* The TEXT_DISTANCE is then used to find x and y offsets from the midpoint so that the new
			* point is still TEXT_DISTANCE away.
			* Consider that the proportionality of the perpendicular line components are the same as the proportionality
			* of the components of the line from midpoint to the new point.
			*/
			double middleLineLength = Math.sqrt ( Math.pow ( deltaX, 2 ) + Math.pow ( deltaY, 2 ) );

			// Consider that Y/R = y/r ==> y = Y*r/R, applied as uppercase is of line, lower is of text displacement.
			// However, our deltaX and deltaY refer to the middle line, not the perpendicular. We apply the
			// perpendicular slope.
			double textY = midpointY - Math.abs ( deltaX ) * TEXT_DISTANCE / middleLineLength;
			//      That is a minus sign so midpoint is above the line.

			double textX;
			if ( slope > 0 ) // Like "\"
				textX = midpointX + Math.abs ( deltaY ) * TEXT_DISTANCE / middleLineLength;
			else // Like "/"
				textX = midpointX - Math.abs ( deltaY ) * TEXT_DISTANCE / middleLineLength;

			return new MiddleTextInfo ( textX, textY, textAngleInDegrees );
		}
	}

	/**
	 * Finds and returns information related to the intersection of a class box and a line segment that terminates
	 * at the center of the class box. This is specifically for a relationship, and the boolean argument allows
	 * reusing code with deviations as necessary.
	 * <p/>
	 * The information returned is that of an <code>IntersectInfo</code> object.
	 *
	 * @param classBox The class box of the intersection.
	 * @param line The line of the intersection.
	 * @param isParent Indication of the status of the class box as a parent or child for this relationship.
	 *
	 * @return The intersection info of an <code>IntersectInfo</code> object.
	 */
	protected IntersectionInfo findClassBoxIntersectionInformation ( ClassBox classBox, Line line, boolean isParent )
	{
		// Get start point's coordinates.
		double lineStartX = line.getStartX ( );
		double lineStartY = line.getStartY ( );
		// Get end point's coordinates.
		double lineEndX = line.getEndX ( );
		double lineEndY = line.getEndY ( );
		// Get class box's location and dimensions.
		double boxX = classBox.getTranslateX ( );
		double boxY = classBox.getTranslateY ( );
		double boxWidth = classBox.getWidth ( );
		double boxHeight = classBox.getHeight ( );
		// Consider a scaled (expanded) class box's boundary - get location and dimensions.
		double psuedoScaleFactor = ( boxHeight + 2 * TEXT_DISTANCE ) / boxHeight;
		double expBoxWidth = boxWidth * psuedoScaleFactor;
		double expBoxHeight = boxHeight * psuedoScaleFactor;
		double expBoxX = boxX - ( expBoxWidth - boxWidth ) / 2;
		double expBoxY = boxY - ( expBoxHeight - boxHeight ) / 2;
		// Get deltas in x and y coordinates.
		double deltaY = lineEndY - lineStartY;
		double deltaX = lineEndX - lineStartX;
		// Get slope.
		double slope = deltaX == 0 ? Double.MAX_VALUE : deltaY / deltaX;

		double intersectX, intersectY;
		double textX, textY;

		/*
		 * This large "if" body collects assesses all cases, and produces relevant information
		 * concerning the the direction that points away from the parent and the the intersection
		 * of the jointedLine with the edge.
		 *
		 * In this block, the explanation refers to two notes:
		 * Note 1:
		 *      For the displacement from center, consider dy/dx = (-height / 2)/ u ==> u = dx * -height / (2 * dy);
		 *      Thus, the jointedLine's slope is applied to the known height to find the unknown x-displacement from
		 *      center.The center is at x + width/2. The sum is the point of intersection, which can be simplified to
		 *      be x + (width - dx * height / dy ) / 2  for one less division by 2 (which is quick, but anyway...).
		 *      Here, dy cannot be 0.
		 * Note 2:
		 *      For the displacement, consider dy/dx = u/(-width/2) ==> u =dy * -width / (2 * dx);  thus the
		 *      jointedLine's slope is applied to the know height to find the unknown y-displacement from center. The
		 *      center is at y + height/2. The sum is the point of intersection, which can be simplified to be
		 *      y + (height - dy * width / dx)/2. Here, dx cannot be 0.
		 */
		// Test to see if the relation will meet on a vertical or horizontal side for the parent class box.
		// abs (deltaY/deltaX) > height /width ==> slope of jointedLine is steeper than ratio - jointedLine hits top or bottom.
		// Equivalently, abs (deltaY) * width > height * abs (deltaX) ==> same thing. But, there's no division!
		if ( Math.abs ( deltaY ) * classBox.getWidth ( ) > classBox.getHeight ( ) * Math.abs ( deltaX ) )
		{
			// Intersection is at top or bottom.
			// Line could be vertical, and deltaY != 0
			// Centers do not overlap.

			if ( lineStartY < lineEndY )
			{
				// Intersection is at top of parent, bottom of child.
				if ( isParent )
				{
					intersectY = boxY;
					textY = expBoxY;
					// See Note 1.
					intersectX = boxX + ( boxWidth - deltaX * boxHeight / deltaY ) / 2;
					textX = expBoxX + ( expBoxWidth - deltaX * expBoxHeight / deltaY ) / 2;
				} else
				{
					intersectY = boxY + boxHeight;
					textY = expBoxY + expBoxHeight;
					// See Note 1.
					intersectX = boxX + ( boxWidth + deltaX * boxHeight / deltaY ) / 2;
					textX = expBoxX + ( expBoxWidth + deltaX * expBoxHeight / deltaY ) / 2;
				}
			} else
			{
				// Intersection is at bottom of parent, top of child.
				if ( isParent )
				{
					intersectY = boxY + boxHeight;
					textY = expBoxY + expBoxHeight;
					// See Note 1.
					intersectX = boxX + ( boxWidth + deltaX * boxHeight / deltaY ) / 2;
					textX = expBoxX + ( expBoxWidth + deltaX * expBoxHeight / deltaY ) / 2;
				} else
				{
					intersectY = boxY;
					textY = expBoxY;
					// See Note 1.
					intersectX = boxX + ( boxWidth - deltaX * boxHeight / deltaY ) / 2;
					textX = expBoxX + ( expBoxWidth - deltaX * expBoxHeight / deltaY ) / 2;
				}
			}
		} else if ( deltaX == 0 && deltaY == 0 )
		{
			// Centers overlap.
			intersectX = lineStartX;
			intersectY = lineStartY;
			textX = boxX + boxWidth / 2;
			textY = boxY + boxHeight / 2;
		} else
		{
			// Intersection is on left or right.
			// Line could be horizontal.
			// deltaX != 0
			if ( lineStartX < lineEndX )
			{
				// Intersection is on left of parent, right of child.
				if ( isParent )
				{
					intersectX = boxX;
					textX = expBoxX;
					// See Note 2.
					intersectY = boxY + ( boxHeight - deltaY * boxWidth / deltaX ) / 2;
					textY = expBoxY + ( expBoxHeight - deltaY * expBoxWidth / deltaX ) / 2;
				} else
				{
					intersectX = boxX + boxWidth;
					textX = expBoxX + expBoxWidth;
					// See Note 2.
					intersectY = boxY + ( boxHeight + deltaY * boxWidth / deltaX ) / 2;
					textY = expBoxY + ( expBoxHeight + deltaY * expBoxWidth / deltaX ) / 2;
				}
			} else
			{
				// Intersection is on right of parent, left of child.
				if ( isParent )
				{
					intersectX = boxX + boxWidth;
					textX = expBoxX + expBoxWidth;
					// See Note 2.
					intersectY = boxY + ( boxHeight + deltaY * boxWidth / deltaX ) / 2;
					textY = expBoxY + ( expBoxHeight + deltaY * expBoxWidth / deltaX ) / 2;
				} else
				{
					intersectX = boxX;
					textX = expBoxX;
					// See Note 2.
					intersectY = boxY + ( boxHeight - deltaY * boxWidth / deltaX ) / 2;
					textY = expBoxY + ( expBoxHeight - deltaY * expBoxWidth / deltaX ) / 2;
				}
			}
		} // End identification of point of intersection.

		return new IntersectionInfo ( intersectX, intersectY, textX, textY, Math.toDegrees ( Math.atan ( slope ) ) );
	}

	/**
	 * This is used by the <code>JointedLine</code> object to format each jointedLine in terms of style and
	 * assigned event handlers.
	 */
	public void formatLine ( Line line )
	{
		line.setOnMousePressed ( getOnMousePressed ( ) );
		line.setOnMouseEntered ( new EventHandler< MouseEvent > ( )
		{
			@Override
			public void handle ( MouseEvent event )
			{
				if ( canvasManager.getCurrentTool ( ) == ToolType.EDIT_TEXT )
				{
					setBoxVisible ( true );
				}
			}
		} );
		line.setOnMouseExited ( new EventHandler< MouseEvent > ( )
		{
			@Override
			public void handle ( MouseEvent event )
			{
				if ( canvasManager.getCurrentTool ( ) == ToolType.EDIT_TEXT )
				{
					setBoxVisible ( false );
				}
			}
		} );
	}

	/**
	 * Changes the cardinality and label relationship text fields to be visible, or invisible as the box allows.
	 *
	 * @param isVisible True if desiring the box to be visible, false otherwise.
	 */
	private void setBoxVisible ( boolean isVisible )
	{
		childCardinality.setBoxVisible ( isVisible );
		parentCardinality.setBoxVisible ( isVisible );
		label.setBoxVisible ( isVisible );
	}

	/**
	 * Sets mouse listeners as per the implementation of the relationship.
	 */
	public abstract void updateMouseEventHandlers ( );

	/** Sets the canvas manager for the relationship. */
	public void setCanvasManager ( CanvasManager canvasManager )
	{
		this.canvasManager = canvasManager;
	}

	/**
	 * Returns the jointed jointedLine for this relationship.
	 *
	 * @return The jointed jointedLine for this relationship.
	 */
	public JointedLine getJointedLine ( )
	{
		return jointedLine;
	}

	/**
	 * Enables editing the text of the cardinalities and label of a relationship.
	 */
	public void enableTextFields ( )
	{
		childCardinality.setMouseTransparent ( false );
		parentCardinality.setMouseTransparent ( false );
		label.setMouseTransparent ( false );
	}

	/**
	 * Disables editing the text of the cardinalities and label of a relationship.
	 */
	public void disableTextFields ( )
	{
		childCardinality.setMouseTransparent ( true );
		parentCardinality.setMouseTransparent ( true );
		label.setMouseTransparent ( true );
	}

	/**
	 * Adds event handlers to handle mouse events.
	 */
	private void addTextFieldHandlers ( )
	{
		TextField generalField = new TextField ( );
		final boolean fieldIsUsed = false;

		EventHandler< MouseEvent > onEnter = new EventHandler< MouseEvent > ( )
		{
			@Override
			public void handle ( MouseEvent event )
			{
				// setVisible ( true );
			}
		};
		EventHandler< MouseEvent > onExit = new EventHandler< MouseEvent > ( )
		{
			@Override
			public void handle ( MouseEvent event )
			{
				//if (!fieldIsUsed)
				//setVisible ( false );
			}
		};
	}
}
