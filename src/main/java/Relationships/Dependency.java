package Relationships;

import BoxObjects.ClassBox;
import javafx.scene.shape.Line;

/**
 * This is characterized by a dashed line leading to a simple arrow ("- - ->").
 */
public class Dependency extends Relationship
{
	Line leftWing; // The line that extends away from the tip on the clockwise side of the line.
	Line rightWing; // The line that extends away from the tip on the counter-clockwise side of the line.

	/**
	 * Constructs a dependency relation.
	 * <p/>
	 * This is characterized by a dashed jointedLine leading to a simple arrow ("- - ->").
	 *
	 * @param child The child of the relationship (tail end).
	 * @param parent The parent of the relationship (head end).
	 */
	public Dependency ( ClassBox child, ClassBox parent )
	{
		super ( child, parent );
		double halfBase = HALF_TRIANGLE_HEIGHT * 2 / Math.sqrt ( 3 );
		// Create the lines.
		leftWing = new Line ( -TRIANGLE_HEIGHT, -HALF_TRIANGLE_BASE, 0, 0 );
		rightWing = new Line ( -TRIANGLE_HEIGHT, HALF_TRIANGLE_BASE, 0, 0 );
		// Format the lines.
		leftWing.setStroke ( LINE_COLOR );
		leftWing.setStrokeWidth ( STROKE_WIDTH );
		rightWing.setStroke ( LINE_COLOR );
		rightWing.setStrokeWidth ( STROKE_WIDTH );

		getChildren ( ).addAll ( leftWing, rightWing );
		update ( );
	}

	@Override
	public void formatLine ( Line line )
	{
		super.formatLine ( line );
		line.setStroke ( LINE_COLOR );
		line.setStrokeWidth ( STROKE_WIDTH );
		line.getStrokeDashArray ( ).setAll ( DASH_LENGTH );
	}

	/**
	 * Sets the arrow lines to also cause removal of this relationship.
	 */
	@Override
	public void updateMouseEventHandlers ( )
	{
		leftWing.setOnMousePressed ( getOnMousePressed ( ) );
		rightWing.setOnMousePressed ( getOnMousePressed ( ) );
		jointedLine.setOnMousePressed ( getOnMousePressed ( ) );
	}
}
