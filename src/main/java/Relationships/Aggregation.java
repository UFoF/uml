package Relationships;

import BoxObjects.ClassBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;

/**
 * This relationship is characterized by a solid line and an empty diamond as the arrowhead.
 */
public class Aggregation extends Relationship
{
	private Rectangle unfilledDiamond; // This is global only to add the handlers.

	/**
	 * Constructs a aggregation relationship.
	 * <p/>
	 * This relationship is characterized by a solid line and an empty diamond as the arrowhead.
	 *
	 * @param child The child of the relationship (tail end).
	 * @param parent The parent of the relationship (head end).
	 */
	public Aggregation ( ClassBox child, ClassBox parent )
	{
		super ( child, parent );
		unfilledDiamond = new Rectangle ( 15, 15, Color.WHITE );
		unfilledDiamond.setStroke ( LINE_COLOR );
		unfilledDiamond.setFill ( Color.WHITE );
		unfilledDiamond.setStrokeWidth ( STROKE_WIDTH );
		unfilledDiamond.getTransforms ( ).add ( new Rotate ( 135, 0, 0 ) );
		getChildren ( ).add ( unfilledDiamond );
		update ( );
	}

	@Override
	public void formatLine ( Line line )
	{
		super.formatLine ( line );
		line.setStroke ( LINE_COLOR );
		line.setStrokeWidth ( STROKE_WIDTH );
	}

	/**
	 * Sets the diamond to also cause removal of this relationship.
	 */
	@Override
	public void updateMouseEventHandlers ( )
	{
		unfilledDiamond.setOnMousePressed ( getOnMousePressed ( ) );
		jointedLine.setOnMousePressed ( getOnMousePressed ( ) );
	}
}