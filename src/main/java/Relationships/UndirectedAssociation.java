package Relationships;

import BoxObjects.ClassBox;
import javafx.scene.shape.Line;

/**
 * This relationship is characterized by a solid line and no arrowhead.
 */
public class UndirectedAssociation extends Relationship
{
	Line line;

	/**
	 * Constructs an undirected association relationship.
	 * <p/>
	 * This relationship is characterized by a solid jointedLine and no arrowhead.
	 * <p/>
	 * The child/parent distinction is irrelevant for this relationship.
	 *
	 * @param child The child of the relationship (tail end).
	 * @param parent The parent of the relationship (head end).
	 */
	public UndirectedAssociation ( ClassBox child, ClassBox parent )
	{
		super ( child, parent );
		line = new Line ( );

		getChildren ( ).add ( line );
	}

	@Override
	public void formatLine ( Line line )
	{
		super.formatLine ( line );
		line.setStroke ( LINE_COLOR );
		line.setStrokeWidth ( STROKE_WIDTH );
	}

	@Override
	public void updateMouseEventHandlers ( )
	{
		line.setOnMousePressed ( getOnMousePressed ( ) );
		line.setOnMousePressed ( getOnMousePressed ( ) );
	}

	@Override
	public void update ( )
	{
		// Do nothing. There is no arrowhead to update.
	}
}
