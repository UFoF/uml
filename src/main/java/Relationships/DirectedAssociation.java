package Relationships;

import BoxObjects.ClassBox;
import javafx.scene.shape.Line;

/**
 * This relationship is characterized by a solid line and a simple arrowhead ("-->").
 */
public class DirectedAssociation extends Relationship
{
	Line leftWing; // The line that extends away from the tip on the clockwise side of the line.
	Line rightWing; // The line that extends away from the tip on the counter-clockwise side of the line.

	/**
	 * Constructs a directed association relationship.
	 * <p/>
	 * This relationship is characterized by a solid jointedLine and a simple arrowhead ("-->").
	 *
	 * @param child The child of the relationship (tail end).
	 * @param parent The parent of the relationship (head end).
	 */
	public DirectedAssociation ( ClassBox child, ClassBox parent )
	{
		super ( child, parent );
		double halfBase = HALF_TRIANGLE_HEIGHT * 2 / Math.sqrt ( 3 );
		// Create the lines.
		leftWing = new Line ( -TRIANGLE_HEIGHT, -HALF_TRIANGLE_BASE, 0, 0 );
		rightWing = new Line ( -TRIANGLE_HEIGHT, HALF_TRIANGLE_BASE, 0, 0 );
		// Format the lines.
		formatLine ( leftWing );
		formatLine ( rightWing );

		getChildren ( ).addAll ( leftWing, rightWing );
		update ( );
	}

	@Override
	public void formatLine ( Line line )
	{
		super.formatLine ( line );
		line.setStroke ( LINE_COLOR );
		line.setStrokeWidth ( STROKE_WIDTH );
	}

	/**
	 * Sets the arrow lines to also cause removal of this relationship.
	 */
	@Override
	public void updateMouseEventHandlers ( )
	{
		leftWing.setOnMousePressed ( getOnMousePressed ( ) );
		rightWing.setOnMousePressed ( getOnMousePressed ( ) );
		jointedLine.setOnMousePressed ( getOnMousePressed ( ) );
	}
}
