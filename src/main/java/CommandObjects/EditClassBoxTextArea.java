package CommandObjects;

import BoxObjects.ClassBox;
import javafx.scene.control.TextArea;

/**
 * This is the command object to undo and redo the change in a class box's text.
 */
public class EditClassBoxTextArea implements CommandObject
{
	ClassBox classBox;
	TextArea textArea;
	String oldText;
	String newText;

	/**
	 * Creates a command object to undo and redo the change in a class box's text.
	 *
	 * @param classBox The class box that has the text area.
	 * @param textArea The text area that has changed.
	 * @param oldText The initial text.
	 * @param newText The new text.
	 */
	public EditClassBoxTextArea ( ClassBox classBox, TextArea textArea, String oldText, String newText )
	{
		this.classBox = classBox;
		this.textArea = textArea;
		this.oldText = oldText;
		this.newText = newText;
	}

	/**
	 * Sets the text of the <code>TextArea</code> to be the new text.
	 */
	@Override
	public void doCommand ( )
	{
		classBox.ignoreChange = true;
		textArea.setText ( newText );
		classBox.ignoreChange = false;
	}

	/**
	 * Sets the text of the <code>TextArea</code> to be the old text.
	 */
	@Override
	public void undoCommand ( )
	{
		classBox.ignoreChange = true;
		textArea.setText ( oldText );
		classBox.ignoreChange = false;
	}
}
