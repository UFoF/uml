package CommandObjects;

import BoxObjects.ClassBox;
import Relationships.Relationship;

import java.util.ArrayList;

/**
 * This is the command object for the destruction of a class box.
 */
public class DestroyObject implements CommandObject
{
	ClassBox classBox;
	ArrayList< Relationship > initialRelationships;

	/**
	 * Constructs a <code>DestroyObject</code> to remove the given <code>ClassBox</code>.
	 *
	 * @param classBox The class box to be destroyed or brought back.
	 */
	public DestroyObject ( ClassBox classBox )
	{
		this.classBox = classBox;
		initialRelationships = classBox.copyRelationships ( );
	}

	/**
	 * Removes the class box and all connected relationships from the canvas.
	 */
	@Override
	public void doCommand ( )
	{
		classBox.removeSelf ( );
	}

	/**
	 * Puts the class box and all of its relationships back onto the canvas.
	 */
	@Override
	public void undoCommand ( )
	{
		classBox.addSelf ( );

		for ( Relationship rel : initialRelationships )
		{
			rel.addSelf ( );
		}
		classBox.updateRelationships ();
	}
}
