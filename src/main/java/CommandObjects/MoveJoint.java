package CommandObjects;

import RelationshipAuxilliary.Joint;
import Relationships.Relationship;

/**
 * This is the command object that manages the action of moving a <code>Joint</code> of a <code>JointedLine</code>.
 */
public class MoveJoint implements CommandObject
{
	private Joint joint; // rectangle that is moved
	private double initialX; // initial x
	private double initialY; // initial y
	private double finalX; // final x
	private double finalY; // final y
	private Relationship relationship;

	/**
	 * Constructs command object for moving a joint with the initial and final coordinates.
	 *
	 * @param joint The rectangle that is to be moved.
	 * @param initialX The initial x coordinate.
	 * @param initialY The initial y coordinate.
	 * @param finalX The final x coordinate.
	 * @param finalY The final y coordinate.
	 */
	public MoveJoint ( Joint joint, double initialX, double initialY, double finalX, double finalY )
	{
		this.joint = joint;
		this.initialX = initialX;
		this.initialY = initialY;
		this.finalX = finalX;
		this.finalY = finalY;
		relationship = joint.getRelationship ();
	}

	/**
	 * Moves the joint to the new location.
	 */
	@Override
	public void doCommand ( )
	{
		joint.setCenterX ( finalX );
		joint.setCenterY ( finalY );
		relationship.update ();
	}

	/**
	 * Restores the joint to its initial location.
	 */
	@Override
	public void undoCommand ( )
	{
		joint.setCenterX ( initialX );
		joint.setCenterY ( initialY );
		relationship.update ();
	}
}
