package CommandObjects;

import java.io.Serializable;
import java.util.Stack;

/**
 * Manages the done and undone commands. It is the goal for this
 * to be the gateway necessary for any command object to be used.
 * <p/>
 * It essentially calls objects to enact themselves as appropriate for an initial, undo, or redo command. This object
 * also controls <code>Stack</code> objects to manage the objects to be executed on any undo or redo command.
 */
public class CommandAcceptor implements Serializable
{
	Stack< CommandObject > doneStack;
	Stack< CommandObject > undoneStack;

	/**
	 * Constructs a <code>CommandAcceptor</code> with its <code>Stack</code> objects.
	 */
	public CommandAcceptor ( )
	{
		doneStack = new Stack< CommandObject > ( );
		undoneStack = new Stack< CommandObject > ( );
	}

	/**
	 * Calls on the command object's <code>doCommand()</code> method and pushes the command onto the
	 * <code>doneStack</code>.
	 *
	 * @param commandObject The command to be enacted.
	 */
	public void acceptCommand ( CommandObject commandObject )
	{
		commandObject.doCommand ( );
		doneStack.push ( commandObject );
		undoneStack.removeAllElements ( ); // When an action is done, it overwrites anything that was
		//      undone and could be done again. That stuff cannot be done again.
	}

	/**
	 * Calls on the command object's <code>undoCommand()</code> method and manages the done/undone stacks.
	 * <p/>
	 * Stack management, more precisely, involves popping the command object to be undone off of the
	 * <code>doneStack</code> and pushing it onto the <code>undoneStack</code>.
	 */
	public void undoCommand ( )
	{
		if ( !doneStack.isEmpty ( ) )
		{
			doneStack.peek ( ).undoCommand ( );
			undoneStack.push ( doneStack.pop ( ) );
		}
	}

	/**
	 * Calls on the command object's <code>doCommand()</code> method and manages the done/undone stacks.
	 * <p/>
	 * Stack management, more precisely, involves popping the command object to be redone off of the
	 * <code>undoneStack</code> and pushing it onto the <code>doneStack</code>.
	 */
	public void redoCommand ( )
	{
		if ( !undoneStack.isEmpty ( ) )
		{
			undoneStack.peek ( ).doCommand ( );
			doneStack.push ( undoneStack.pop ( ) );
		}
	}
}
