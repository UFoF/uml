package CommandObjects;

import BoxObjects.ClassBox;

/**
 * Embodies the commands for the creation of a class box.
 */
public class CreateObject implements CommandObject
{
	ClassBox classBox;

	/**
	 * Constructs the object that will manage the doing and undoing of class box application to the canvas.
	 *
	 * @param classBox The <code>ClassBox</code> that this object will manage.
	 */
	public CreateObject ( ClassBox classBox )
	{
		this.classBox = classBox;
	}

	/**
	 * Calls on the class box to add itself.
	 * <p/>
	 * It does this through the <code>Utility</code> class.
	 */
	@Override
	//this sure does commands alright I tell ya what
	// Alright....
	public void doCommand ( )
	{
		classBox.addSelf ( );
	}

	/**
	 * Calls on the class box to remove itself.
	 * <p/>
	 * It does this through the <code>Utility</code> class.
	 */
	@Override
	public void undoCommand ( )
	{
		classBox.removeSelf ( );
	}
}
