package CommandObjects;

import Relationships.Relationship;

/**
 * This object embodies the command to create and take away a particular UML class diagram relationship.
 */
public class CreateRelationship implements CommandObject
{
	Relationship relationship;

	/**
	 * Constructs the <code>CreateRelationship</code> object for the purpose of creating and un-creating the given
	 * relationship.
	 *
	 * @param relationship The relationship that this object will create and un-create.
	 */
	public CreateRelationship ( Relationship relationship )
	{
		this.relationship = relationship;
	}

	/**
	 * Causes the relationship to add itself to the its class boxes and canvas.
	 * <p/>
	 * It does this using the <code>Utility</code> class.
	 */
	@Override
	public void doCommand ( )
	{
		// TODO TEST Test for null relationship.
		Utility.createRelationship ( relationship );
	}

	/**
	 * Causes the relationship to remove itself from the its class boxes and canvas.
	 * <p/>
	 * It does this using the <code>Utility</code> class.
	 */
	@Override
	public void undoCommand ( )
	{
		// TODO TEST Test for null relationship.
		Utility.destroyRelationship ( relationship );
	}
}
