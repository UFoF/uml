package CommandObjects;

import Relationships.Relationship;

/**
 * This is a utility class for the command objects.
 * <p/>
 * This class holds static methods for the creation and destruction of class boxes and relationships.
 * The purpose of this segregation is to eliminate repeat code.
 */
public class Utility
{
	/**
	 * Called whenever a relationship must be created.
	 *
	 * @param relationship The <code>Relationship</code> to create.
	 */
	static void createRelationship ( Relationship relationship )
	{
		relationship.addSelf ( );
	}

	/**
	 * Called whenever a relationship must be destroyed.
	 */
	static void destroyRelationship ( Relationship relationship )
	{
		relationship.removeSelf ( );
	}
}
