package CommandObjects;

import Relationships.Relationship;

/**
 * This is the command object for the destruction of a relationship.
 */
public class DestroyRelationship implements CommandObject
{
	Relationship relationship;

	public DestroyRelationship ( Relationship relationship )
	{
		this.relationship = relationship;
	}

	/**
	 * Removes the relationship from the class boxes.
	 */
	@Override
	public void doCommand ( )
	{
		// TODO TEST Test for null relationship.
		Utility.destroyRelationship ( relationship );
	}

	/**
	 * Adds the relationship to the class boxes.
	 */
	@Override
	public void undoCommand ( )
	{
		// TODO TEST Test for null relationship.
		Utility.createRelationship ( relationship );
	}
}
