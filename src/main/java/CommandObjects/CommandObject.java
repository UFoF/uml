package CommandObjects;

import java.io.Serializable;

/**
 * Interface necessary for a doable and undoable command.
 */
public interface CommandObject extends Serializable
{
	// TODO Consider having a separate redo command? In the case that nothing needs done initially?

	/**
	 * Called to enact the command embodied in the command object.
	 */
	void doCommand ( );

	/**
	 * Called to undo the embodied command.
	 */
	void undoCommand ( );
}