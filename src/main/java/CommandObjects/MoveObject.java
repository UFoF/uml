package CommandObjects;

import BoxObjects.ClassBox;

/**
 * Command object for undoing and redoing the change of the location of an object.
 */
public class MoveObject implements CommandObject
{
	// TODO TEST Manually undo and redo movement to see if if goes back to the correct spot -
	// TODO CONTINUED there might be an issue with translating by int values
	// Although, we should be translating by integer values by dragging, also. The floating point
	// arithmetic might just do something odd, like having a subtraction be 1.99999 -> 1 where it
	// should be 2.
	ClassBox reference; // rectangle that is moved
	private double initialX; // initial x
	private double initialY; // initial y
	private double finalX; // final x
	private double finalY; // final y

	/**
	 * Constructs a move command object with the initial and final coordinates.
	 *
	 * @param reference The rectangle that is to be moved.
	 * @param initialX The initial x coordinate.
	 * @param initialY The initial y coordinate.
	 * @param finalX The final x coordinate.
	 * @param finalY The final y coordinate.
	 */
	public MoveObject ( ClassBox reference, double initialX, double initialY, double finalX, double finalY )
	{
		this.reference = reference;
		this.initialX = initialX;
		this.initialY = initialY;
		this.finalX = finalX;
		this.finalY = finalY;
	}

	/**
	 * Translates the rectangle to the position that it was dragged to.
	 * <p>
	 *
	 * This makes the final position of the drag equivalent to the position of the box
	 * after every undo/redo pair.
	 */
	public void doCommand ( )
	{
		reference.setTranslateX ( finalX );
		reference.setTranslateY ( finalY );
		reference.updateRelationships ( );
	}

	/**
	 * Translates the rectangle to the position that it was most recently dragged from.
	 */
	public void undoCommand ( )
	{
		reference.setTranslateX ( initialX );
		reference.setTranslateY ( initialY );
		reference.updateRelationships ( );
	}
}
