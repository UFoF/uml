package CommandObjects;

import RelationshipAuxilliary.RelationshipTextField;

/**
 * This command object is for editing the text of a relationship. They are the cardinalities and label for the
 * relationship.
 */
public class EditRelationshipText implements CommandObject
{
	RelationshipTextField relTextField;
	String initialText;
	String endText;

	/**
	 * Constructs the command object with the necessary information: the text before and after the change and the
	 * <code>RelationshipTextField</code> that changed.
	 *
	 * @param relTextField
	 * @param initialText
	 * @param endText
	 */
	public EditRelationshipText ( RelationshipTextField relTextField, String initialText, String endText )
	{
		this.relTextField = relTextField;
		this.initialText = initialText;
		this.endText = endText;
	}

	/**
	 * Sets the text of the <code>RelationshipTextField</code> to the latter text.
	 */
	@Override
	public void doCommand ( )
	{
		relTextField.ignoreChange = true;
		relTextField.setText ( endText );
		relTextField.refreshVisibility ( );
		relTextField.refreshSize ( );
		relTextField.ignoreChange = false;
	}

	/**
	 * Sets the text of the <code>RelationshipTextField</code> to the former text.
	 */
	@Override
	public void undoCommand ( )
	{
		relTextField.ignoreChange = true;
		relTextField.setText ( initialText );
		relTextField.refreshVisibility ( );
		relTextField.refreshSize ( );
		relTextField.ignoreChange = false;
	}
}
