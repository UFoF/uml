package CommandObjects;

import RelationshipAuxilliary.Joint;
import RelationshipAuxilliary.JointedLine;
import javafx.scene.shape.Line;

/**
 * This object manages the action of creating a <code>Joint</code> in a <code>JointedLine</code>.
 */
public class CreateJoint implements CommandObject
{
	JointedLine jointedLine;
	Line lineIntersected;
	double xPosition;
	double yPosition;
	Joint jointCreated;

	/**
	 * Creates the command object for doing and undoing the creation of joints in relationship lines.
	 */
	public CreateJoint ( JointedLine jointedLine, Line lineIntersected, double xPosition, double yPosition )
	{
		this.jointedLine = jointedLine;
		this.lineIntersected = lineIntersected;
		this.xPosition = xPosition;
		this.yPosition = yPosition;
	}

	/**
	 * Adds a <code>Joint</code> to the jointed line and saves that joint for "undo" use.
	 */
	@Override
	public void doCommand ( )
	{
		jointCreated = jointedLine.addJoint ( lineIntersected, xPosition, yPosition );
	}

	/**
	 * Removes a joint from the <code>JointedLine</code> of this object by calling the <code>removeJoint</code>
	 * method of the <code>JointedLine</code>.
	 */
	@Override
	public void undoCommand ( )
	{
		jointedLine.removeJoint ( jointCreated );
	}
}
