package CommandObjects;

import RelationshipAuxilliary.Joint;
import RelationshipAuxilliary.JointedLine;
import javafx.scene.shape.Line;

/**
 * Command object for deleting a joint.
 */
public class DestroyJoint implements CommandObject
{
	Joint jointDestroyed;
	Line lineIntersected;
	JointedLine jointedLine;

	/**
	 * Constructs this command object to handle the destruction and recreation of the given joint of the given line.
	 *
	 * @param jointDestroyed
	 */
	public DestroyJoint ( Joint jointDestroyed )
	{
		this.jointDestroyed = jointDestroyed;
		this.lineIntersected = jointDestroyed.getPrevious ( );
		this.jointedLine = jointDestroyed.getRelationship ( ).getJointedLine ( );
	}

	/**
	 * Removes the joint from the diagram as per the <code>removeJoint (...)</code> method of <code>JointedLine</code>.
	 */
	@Override
	public void doCommand ( )
	{
		jointedLine.removeJoint ( jointDestroyed );
	}

	/**
	 * Adds an identical joint to the line, and save the reference for a "redo" action.
	 */
	@Override
	public void undoCommand ( )
	{
		double jointX = jointDestroyed.getCenterX ( );
		double jointY = jointDestroyed.getCenterY ( );
		jointDestroyed = jointedLine.addJoint ( lineIntersected, jointX, jointY );
	}
}
